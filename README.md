# Iepen Mienskipfûns - PRA Model

## Description
This repository contains the Persoonlijke Regelingen Assistent Model (PRA model) for the Iepen Mienskipfûns use case (Privincie Friesland)
More about PRA models, what they are and how they are used can be found here: https://gitlab.com/discipl/PRA/algemeen/-/blob/main/PRA-models.md
