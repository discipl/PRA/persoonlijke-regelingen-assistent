<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ba988f2d-41d6-487a-a8aa-fcf6f688bf35(Mienskip)">
  <persistence version="9" />
  <languages>
    <use id="69940819-10c1-4a38-ac44-700b63f993ba" name="Flint" version="14" />
    <use id="8ca79d43-eb45-4791-bdd4-0d6130ff895b" name="de.itemis.mps.editor.diagram.layout" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="92d2ea16-5a42-4fdf-a676-c7604efe3504" name="de.slisson.mps.richtext">
      <concept id="2557074442922380897" name="de.slisson.mps.richtext.structure.Text" flags="ng" index="19SGf9">
        <child id="2557074442922392302" name="words" index="19SJt6" />
      </concept>
      <concept id="2557074442922438156" name="de.slisson.mps.richtext.structure.Word" flags="ng" index="19SUe$">
        <property id="2557074442922438158" name="escapedValue" index="19SUeA" />
      </concept>
    </language>
    <language id="69940819-10c1-4a38-ac44-700b63f993ba" name="Flint">
      <concept id="6868897032739893314" name="Flint.structure.IExplainable" flags="ng" index="cog$R">
        <property id="778381075952164307" name="explanation" index="3ANC2_" />
      </concept>
      <concept id="6868897032739893310" name="Flint.structure.FlintSource" flags="ng" index="cog_b">
        <property id="1495612730333317137" name="textSourceName" index="2XObfb" />
        <property id="2215264714367931041" name="textId" index="1hTq4$" />
        <property id="1165398171153094508" name="language" index="1tl0gq" />
        <child id="7816114204006679678" name="betterText" index="2hN6Sa" />
      </concept>
      <concept id="6868897032739434615" name="Flint.structure.FlintModel" flags="ng" index="cu0$2" />
      <concept id="6868897032739434618" name="Flint.structure.Fact" flags="ng" index="cu0$f">
        <child id="6868897032739751036" name="function" index="coNO9" />
      </concept>
      <concept id="2444626260293387291" name="Flint.structure.Duty" flags="ng" index="2cz0EU">
        <child id="6205025464253210169" name="claimant" index="3H37fL" />
        <child id="6205025464253210160" name="dutyHolder" index="3H37fS" />
      </concept>
      <concept id="2444626260293394822" name="Flint.structure.DutyReference" flags="ng" index="2cz2WB">
        <reference id="2444626260293394823" name="duty" index="2cz2WA" />
      </concept>
      <concept id="7816114204010268258" name="Flint.structure.TaggedWord" flags="ng" index="2h$EKm">
        <child id="7816114204010268263" name="roles" index="2h$EKj" />
      </concept>
      <concept id="7816114204006345028" name="Flint.structure.CustomText" flags="ng" index="2hPCcK" />
      <concept id="9029403747833789403" name="Flint.structure.Act" flags="ng" index="mu5$5">
        <child id="9029403747833803225" name="terminate" index="mu1c7" />
        <child id="9029403747833803217" name="create" index="mu1cf" />
        <child id="9029403747833797790" name="preconditions" index="mu3T0" />
        <child id="591807039346570203" name="action" index="3FTnq6" />
        <child id="6205025464253204623" name="object" index="3H36l7" />
        <child id="6205025464253204638" name="recipient" index="3H36lm" />
        <child id="6205025464253204596" name="actor" index="3H36mW" />
      </concept>
      <concept id="6983418503075280677" name="Flint.structure.IHasSources" flags="ng" index="2pmM45">
        <child id="6983418503075280678" name="sources" index="2pmM46" />
      </concept>
      <concept id="900714954669859736" name="Flint.structure.IHasVersionAndValidation" flags="ng" index="2Cxlzy">
        <property id="900714954669872883" name="version" index="2CxiQ9" />
      </concept>
      <concept id="589729100932390229" name="Flint.structure.IHasLanguage" flags="ng" index="ITzSF">
        <child id="5326288789495449519" name="translatedNames" index="1GVO30" />
      </concept>
      <concept id="2986354165693918736" name="Flint.structure.SRole" flags="ng" index="2UK0tq">
        <property id="2986354165693918737" name="role" index="2UK0tr" />
      </concept>
      <concept id="4808965957220771074" name="Flint.structure.AND" flags="ng" index="1zEWgd" />
      <concept id="4808965957220776525" name="Flint.structure.OR" flags="ng" index="1zEXH2" />
      <concept id="4808965957220776522" name="Flint.structure.NOT" flags="ng" index="1zEXH5" />
      <concept id="4808965957220331692" name="Flint.structure.MultiExpression" flags="ng" index="1zF96z">
        <child id="4808965957220331693" name="operands" index="1zF96y" />
      </concept>
      <concept id="4808965957220331688" name="Flint.structure.SingleExpression" flags="ng" index="1zF96B">
        <child id="4808965957220331689" name="operand" index="1zF96A" />
      </concept>
      <concept id="6587498613242404529" name="Flint.structure.FactReference" flags="ng" index="1FQA6B">
        <reference id="6587498613242404530" name="fact" index="1FQA6$" />
      </concept>
      <concept id="5326288789495450601" name="Flint.structure.TranslatedName" flags="ng" index="1GVOM6">
        <property id="5326288789495451684" name="translatedName" index="1GVPtb" />
        <property id="5326288789495451682" name="language" index="1GVPtd" />
      </concept>
      <concept id="491685697582670580" name="Flint.structure.CREATE" flags="ng" index="1RnfdX" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="cu0$2" id="7614aO2F_iU">
    <property role="TrG5h" value="Mienskip" />
  </node>
  <node concept="mu5$5" id="7614aO2F_iV">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="indienen aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3ANC2_" value="Zijn de voorwaarden voorwaarden of plichten verbonden met de aanvraag?" />
    <node concept="1GVOM6" id="7614aO2F_iW" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="indienen aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="cog_b" id="7614aO2F_j1" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="7614aO2F_j2" role="2hN6Sa">
        <node concept="19SUe$" id="7614aO2F_j3" role="19SJt6">
          <property role="19SUeA" value="Een aanvraag wordt " />
        </node>
        <node concept="2h$EKm" id="7614aO2F_k4" role="19SJt6">
          <property role="19SUeA" value="ingediend" />
          <node concept="2UK0tq" id="7614aO2F_k5" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
        </node>
        <node concept="19SUe$" id="7614aO2F_k3" role="19SJt6">
          <property role="19SUeA" value=" binnen de in het openstellingsbesluit vastgestelde aanvraagperiode." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="7614aO2F_j4" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 2 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="7614aO2F_j5" role="2hN6Sa">
        <node concept="19SUe$" id="7614aO2F_j6" role="19SJt6">
          <property role="19SUeA" value="De aanvraag is ingediend met het door gedeputeerde staten vastgestelde aanvraagformulier." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="7614aO2F_jc" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 3 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="7614aO2F_jd" role="2hN6Sa">
        <node concept="19SUe$" id="7614aO2F_je" role="19SJt6">
          <property role="19SUeA" value="Per " />
        </node>
        <node concept="2h$EKm" id="7614aO2F_jB" role="19SJt6">
          <property role="19SUeA" value="aanvrager" />
          <node concept="2UK0tq" id="7614aO2F_jC" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="7614aO2F_jA" role="19SJt6">
          <property role="19SUeA" value=" worden per kalenderjaar maximaal drie aanvragen in behandeling genomen." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="7614aO2F_jD" role="3H36mW">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_k_" role="3FTnq6">
      <ref role="1FQA6$" node="7614aO2F_kz" resolve="indienen" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_nX" role="3H36l7">
      <ref role="1FQA6$" node="7614aO2F_nY" resolve="aanvraagformulier subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_oD" role="3H36lm">
      <ref role="1FQA6$" node="7614aO2F_oB" resolve="gedeputeerde staten" />
    </node>
    <node concept="1zEWgd" id="6dPxXIRBJ_B" role="mu3T0">
      <node concept="1FQA6B" id="6dPxXIRBJAp" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBJAn" resolve="aanvraag is ingediend binnen de in het openstellingsbesluit vastgestelde aanvraagperiode" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBJAy" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBJAw" resolve="aanvraag is ingediend met het door gedeputeerde staten vastgestelde aanvraagformulier" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBJAF" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBJAD" resolve="van aanvrager zijn per kalenderjaar maximaal drie aanvragen in behandeling genomen" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL3c" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL3a" resolve="aanvraag heeft betrekking op een gebied" />
      </node>
    </node>
    <node concept="1FQA6B" id="6dPxXIRBKp5" role="mu1cf">
      <ref role="1FQA6$" node="6dPxXIRBKp2" resolve="aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$j3" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVz8e" resolve="plicht tot schriftelijk indienen aanvraag " />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$j4" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzf1" resolve="wordt ondertekend" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$j5" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzhO" resolve="plicht tot vermelden naam en het adres van de aanvrager" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$j6" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzkH" resolve="plicht tot vermelden dagtekening aanvraag" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$j7" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzmA" resolve="plicht tot het vermelden van de aanduiding van het besluit dat wordt gevraagd" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$j8" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzFw" resolve="plicht tot verschaffen gegevens en bescheiden die voor de beslissing op de aanvraag nodig zijn" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$j9" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzLN" resolve="plicht bestuursorgaan tot vergaren van de nodige kennis omtrent de relevante feiten en de af te wegen belangen" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$ja" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzUp" resolve="plicht tot deugdelijk motiveren besluit" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$jb" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEV$0r" resolve="plicht tot nemen besluit binnen de bij wettelijk voorschrift bepaalde termijn" />
    </node>
  </node>
  <node concept="cu0$f" id="7614aO2F_jE">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvrager" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="7614aO2F_jF" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvrager" />
    </node>
    <node concept="cog_b" id="7614aO2F_jM" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 3 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="7614aO2F_jN" role="2hN6Sa">
        <node concept="19SUe$" id="7614aO2F_jO" role="19SJt6">
          <property role="19SUeA" value="Per " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJkj" role="19SJt6">
          <property role="19SUeA" value="&#10;aanvrager" />
          <node concept="2UK0tq" id="6dPxXIRBJkk" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJki" role="19SJt6">
          <property role="19SUeA" value=" worden per kalenderjaar maximaal drie aanvragen in behandeling genomen." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJo5" role="2pmM46">
      <property role="2XObfb" value="Artikel 10 lid 3 onderdeel 7°. Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJo6" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJo7" role="19SJt6">
          <property role="19SUeA" value="Loonkosten voor zover dit eigen uren van de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJo_" role="19SJt6">
          <property role="19SUeA" value="aanvrager" />
          <node concept="2UK0tq" id="6dPxXIRBJoA" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJo$" role="19SJt6">
          <property role="19SUeA" value=" betreffen." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJoB" role="2pmM46">
      <property role="2XObfb" value="Artikel 10 lid 6 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJoC" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJoD" role="19SJt6">
          <property role="19SUeA" value="Omzetbelasting is slechts subsidiabel voor zover deze voor de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJph" role="19SJt6">
          <property role="19SUeA" value="aanvrager" />
          <node concept="2UK0tq" id="6dPxXIRBJpi" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJpg" role="19SJt6">
          <property role="19SUeA" value=" niet verrekenbaar is." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVz9W" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 2 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVz9X" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz9Y" role="19SJt6">
          <property role="19SUeA" value="De " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz9Z" role="19SJt6">
          <property role="19SUeA" value="aanvrager" />
          <node concept="2UK0tq" id="6OGuHoEVza0" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSnk_W/Holder" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzaG" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVza1" role="19SJt6">
          <property role="19SUeA" value=" verschaft voorts de gegevens en bescheiden die voor de beslissing op de aanvraag nodig zijn en waarover hij redelijkerwijs de beschikking kan krijgen." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzck" role="coNO9">
      <ref role="1FQA6$" node="6OGuHoEVzci" resolve="belanghebbende die bestuursorgaan verzoekt tot het nemen van een besluit" />
    </node>
  </node>
  <node concept="cu0$f" id="7614aO2F_kz">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="indienen" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="7614aO2F_k$" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="indienen" />
    </node>
    <node concept="cog_b" id="7614aO2F_mL" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="7614aO2F_mM" role="2hN6Sa">
        <node concept="19SUe$" id="7614aO2F_mN" role="19SJt6">
          <property role="19SUeA" value="Een aanvraag wordt " />
        </node>
        <node concept="2h$EKm" id="7614aO2F_mO" role="19SJt6">
          <property role="19SUeA" value="ingediend" />
          <node concept="2UK0tq" id="7614aO2F_mP" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="7614aO2F_mQ" role="19SJt6">
          <property role="19SUeA" value=" binnen de in het openstellingsbesluit vastgestelde aanvraagperiode." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="7614aO2F_mR" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 2 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="7614aO2F_mS" role="2hN6Sa">
        <node concept="19SUe$" id="7614aO2F_mT" role="19SJt6">
          <property role="19SUeA" value="De aanvraag wordt " />
        </node>
        <node concept="2h$EKm" id="7614aO2F_mU" role="19SJt6">
          <property role="19SUeA" value="ingediend" />
          <node concept="2UK0tq" id="7614aO2F_mV" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="7614aO2F_mW" role="19SJt6">
          <property role="19SUeA" value=" met het door gedeputeerde staten vastgestelde aanvraagformulier." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="7614aO2F_nY">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvraagformulier subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="7614aO2F_nZ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvraagformulier subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKlz" role="2pmM46">
      <property role="2XObfb" value="Artikel 2 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKl$" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKl_" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten kunnen " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKnv" role="19SJt6">
          <property role="19SUeA" value="ter" />
          <node concept="2UK0tq" id="6dPxXIRBKnw" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKnu" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKnr" role="19SJt6">
          <property role="19SUeA" value="bevordering" />
          <node concept="2UK0tq" id="6dPxXIRBKns" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKnq" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKnn" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBKno" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKnm" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKnj" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBKnk" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKni" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKnf" role="19SJt6">
          <property role="19SUeA" value="leefbaarheid" />
          <node concept="2UK0tq" id="6dPxXIRBKng" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKne" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKnb" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBKnc" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKna" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKn7" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBKn8" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKn6" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKn3" role="19SJt6">
          <property role="19SUeA" value="provincie" />
          <node concept="2UK0tq" id="6dPxXIRBKn4" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKn2" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKmZ" role="19SJt6">
          <property role="19SUeA" value="Fryslân" />
          <node concept="2UK0tq" id="6dPxXIRBKn0" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKmY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKmV" role="19SJt6">
          <property role="19SUeA" value="subsidie" />
          <node concept="2UK0tq" id="6dPxXIRBKmW" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKmU" role="19SJt6">
          <property role="19SUeA" value=" verstrekken voor:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBKlE" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 2 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKlF" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKlG" role="19SJt6">
          <property role="19SUeA" value="De aanvraag wordt ingediend met het door gedeputeerde staten vastgestelde " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKlZ" role="19SJt6">
          <property role="19SUeA" value="aanvraagformulier." />
          <node concept="2UK0tq" id="6dPxXIRBKm0" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKmm" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="7614aO2F_oB">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="gedeputeerde staten" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="7614aO2F_oC" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="gedeputeerde staten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJSz" role="2pmM46">
      <property role="2XObfb" value="Artikel 2 lid 1, aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJS$" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBJSV" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBJSW" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJSU" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJSS" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBJST" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJSR" role="19SJt6">
          <property role="19SUeA" value=" kunnen ter bevordering van de leefbaarheid in de provincie Fryslân subsidie verstrekken voor:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJT6" role="2pmM46">
      <property role="2XObfb" value="Artikel 4, aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJT7" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJT8" role="19SJt6">
          <property role="19SUeA" value="Ten behoeve van de subsidieverstrekking hanteren gedeputeerde staten de volgende gebiedsindeling:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJTf" role="2pmM46">
      <property role="2XObfb" value="Artikel 5 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJTg" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBJUH" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBJUI" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJUG" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJUE" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBJUF" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJUD" role="19SJt6">
          <property role="19SUeA" value=" stellen per gebied als bedoeld in artikel 4 een openstellingsbesluit vast." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJTy" role="2pmM46">
      <property role="2XObfb" value="Artikel 5 lid 2, tweede zin Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJTz" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBJUo" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBJUp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJUn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJUl" role="19SJt6">
          <property role="19SUeA" value="Staten" />
          <node concept="2UK0tq" id="6dPxXIRBJUm" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJUk" role="19SJt6">
          <property role="19SUeA" value=" kunnen de hoogte van het subsidieplafond per gebied na sluiting van de openstellingsperiode wijzigen." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJUS" role="2pmM46">
      <property role="2XObfb" value="Artikel 5 lid 3, aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJUT" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBK16" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBK17" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK15" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK13" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBK14" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK12" role="19SJt6">
          <property role="19SUeA" value=" kunnen in het openstellingsbesluit nadere regels stellen met betrekking tot:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJUV" role="2pmM46">
      <property role="2XObfb" value="Artikel 5 lid 4 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJUW" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBK1o" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBK1p" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK1n" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK1l" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBK1m" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK1k" role="19SJt6">
          <property role="19SUeA" value=" kunnen ten behoeve van de openstelling advies vragen aan de in artikel 9 bedoelde adviescommissies." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJVx" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 2 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJVy" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJVz" role="19SJt6">
          <property role="19SUeA" value="De aanvraag wordt ingediend met het door " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK1F" role="19SJt6">
          <property role="19SUeA" value="gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBK1G" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK1E" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK1B" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBK1C" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK1A" role="19SJt6">
          <property role="19SUeA" value=" vastgestelde aanvraagformulier." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJWa" role="2pmM46">
      <property role="2XObfb" value="Artikel 7 onderdeel j Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJWb" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJWc" role="19SJt6">
          <property role="19SUeA" value="de aanvraag betrekking heeft op activiteiten die worden uitgesloten van subsidie op grond van beleid van provinciale staten of " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK22" role="19SJt6">
          <property role="19SUeA" value="gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBK23" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK21" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK1Y" role="19SJt6">
          <property role="19SUeA" value="staten;" />
          <node concept="2UK0tq" id="6dPxXIRBK1Z" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK1X" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJWQ" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1, aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJWR" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBK2o" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBK2p" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK2n" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK2l" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBK2m" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK2k" role="19SJt6">
          <property role="19SUeA" value=" rangschikken de voor subsidieverlening in aanmerking komende aanvragen per gebied zodanig dat een aanvraag hoger gerangschikt wordt naarmate die naar hun oordeel meer voldoet aan de volgende criteria:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJX_" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 5 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJXA" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBK3b" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBK3c" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK3a" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK38" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBK39" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK37" role="19SJt6">
          <property role="19SUeA" value=" verdelen het beschikbare bedrag in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJYn" role="2pmM46">
      <property role="2XObfb" value="Artikel 9 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJYo" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBK3J" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBK3K" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK3I" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK3G" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBK3H" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK3F" role="19SJt6">
          <property role="19SUeA" value=" stellen per gebied adviescommissies in die in elk geval tot taak hebben te adviseren&#10;over de rangschikking van aanvragen." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJZc" role="2pmM46">
      <property role="2XObfb" value="Artikel 9 lid 2 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJZd" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBK3t" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBK3u" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK3s" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK3q" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBK3r" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK3p" role="19SJt6">
          <property role="19SUeA" value=" stellen voor de adviescommissies een reglement vast." />
        </node>
      </node>
    </node>
  </node>
  <node concept="mu5$5" id="7614aO2F_pW">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="verstrekken subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3ANC2_" value="De act 'verdelen van het beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân' heeft als resultaat een besluit met daarop de aanvragen die subsidie ontvangen. Op basis daarvan kan worden bepaald welke aanvragen subsidie ontvangen. Dit staat niet expliciet in de regeling.&#10;&#10;Onze interpretatie is het dat de lijst aanvragen bevat, en dat deze lijst kan worden gebruikt voor het toekennen van subsidie aan individuele aanvragen." />
    <node concept="1GVOM6" id="7614aO2F_pX" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="verstrekken subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_q7" role="3FTnq6">
      <ref role="1FQA6$" node="7614aO2F_q5" resolve="verstrekken" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_rl" role="3H36mW">
      <ref role="1FQA6$" node="7614aO2F_oB" resolve="gedeputeerde staten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJtI" role="2pmM46">
      <property role="2XObfb" value="Artikel 2 lid 1, aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJtJ" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBJuc" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBJud" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJub" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJu9" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBJua" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJu8" role="19SJt6">
          <property role="19SUeA" value=" kunnen " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJvg" role="19SJt6">
          <property role="19SUeA" value="ter" />
          <node concept="2UK0tq" id="6dPxXIRBJvh" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJvf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJvc" role="19SJt6">
          <property role="19SUeA" value="bevordering" />
          <node concept="2UK0tq" id="6dPxXIRBJvd" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJvb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJv8" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBJv9" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJv7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJv4" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBJv5" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJv3" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJv0" role="19SJt6">
          <property role="19SUeA" value="leefbaarheid" />
          <node concept="2UK0tq" id="6dPxXIRBJv1" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJuZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJuW" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBJuX" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJuV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJuS" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBJuT" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJuR" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJuO" role="19SJt6">
          <property role="19SUeA" value="provincie" />
          <node concept="2UK0tq" id="6dPxXIRBJuP" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJuN" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJuK" role="19SJt6">
          <property role="19SUeA" value="Fryslân" />
          <node concept="2UK0tq" id="6dPxXIRBJuL" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJuJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJuG" role="19SJt6">
          <property role="19SUeA" value="subsidie" />
          <node concept="2UK0tq" id="6dPxXIRBJuH" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJuF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJup" role="19SJt6">
          <property role="19SUeA" value="verstrekken" />
          <node concept="2UK0tq" id="6dPxXIRBJuq" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJuo" role="19SJt6">
          <property role="19SUeA" value=" voor:" />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJvi" role="3H36l7">
      <ref role="1FQA6$" node="6dPxXIRBKp2" resolve="aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJxl" role="3H36lm">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBKpH" role="mu1cf">
      <ref role="1FQA6$" node="6dPxXIRBKpE" resolve="besluit tot verstrekken subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBKpS" role="mu1c7">
      <ref role="1FQA6$" node="6dPxXIRBKp2" resolve="aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVyW_" role="mu3T0">
      <ref role="1FQA6$" node="6OGuHoEVyWz" resolve="aanvraag staat op besluit tot verdeling beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="7614aO2F_q5">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="verstrekken" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="7614aO2F_q6" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="verstrekken" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKgy" role="2pmM46">
      <property role="2XObfb" value="Artikel 2 lid 1, aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKgz" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBKg$" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBKg_" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgA" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgB" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBKgC" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgD" role="19SJt6">
          <property role="19SUeA" value=" kunnen " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgE" role="19SJt6">
          <property role="19SUeA" value="ter" />
          <node concept="2UK0tq" id="6dPxXIRBKgF" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgG" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgH" role="19SJt6">
          <property role="19SUeA" value="bevordering" />
          <node concept="2UK0tq" id="6dPxXIRBKgI" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgK" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBKgL" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgM" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgN" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBKgO" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgP" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgQ" role="19SJt6">
          <property role="19SUeA" value="leefbaarheid" />
          <node concept="2UK0tq" id="6dPxXIRBKgR" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgS" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgT" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBKgU" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgW" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBKgX" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKgY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKgZ" role="19SJt6">
          <property role="19SUeA" value="provincie" />
          <node concept="2UK0tq" id="6dPxXIRBKh0" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKh1" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKh2" role="19SJt6">
          <property role="19SUeA" value="Fryslân" />
          <node concept="2UK0tq" id="6dPxXIRBKh3" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKh4" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKh5" role="19SJt6">
          <property role="19SUeA" value="subsidie" />
          <node concept="2UK0tq" id="6dPxXIRBKh6" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKh7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKh8" role="19SJt6">
          <property role="19SUeA" value="verstrekken" />
          <node concept="2UK0tq" id="6dPxXIRBKh9" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
          <node concept="2UK0tq" id="6dPxXIRBKku" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKha" role="19SJt6">
          <property role="19SUeA" value=" voor:" />
        </node>
      </node>
    </node>
  </node>
  <node concept="mu5$5" id="7614aO2F_qf">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="weigeren aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân door gedeputeerde staten" />
    <node concept="1GVOM6" id="7614aO2F_qg" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="weigeren aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân door gedeputeerde staten" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_qq" role="3FTnq6">
      <ref role="1FQA6$" node="7614aO2F_qo" resolve="geweigerd" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJxD" role="2pmM46">
      <property role="2XObfb" value="Artikel 2, aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJxE" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBJyB" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBJyC" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJyA" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJy$" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBJy_" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJyz" role="19SJt6">
          <property role="19SUeA" value=" kunnen " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$S" role="19SJt6">
          <property role="19SUeA" value="ter" />
          <node concept="2UK0tq" id="6dPxXIRBJ$T" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$R" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$O" role="19SJt6">
          <property role="19SUeA" value="bevordering" />
          <node concept="2UK0tq" id="6dPxXIRBJ$P" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$N" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$K" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBJ$L" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$J" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$G" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBJ$H" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$F" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$C" role="19SJt6">
          <property role="19SUeA" value="leefbaarheid" />
          <node concept="2UK0tq" id="6dPxXIRBJ$D" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$B" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$$" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBJ$_" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$z" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$w" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBJ$x" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$v" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$s" role="19SJt6">
          <property role="19SUeA" value="provincie" />
          <node concept="2UK0tq" id="6dPxXIRBJ$t" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$r" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$o" role="19SJt6">
          <property role="19SUeA" value="Fryslân" />
          <node concept="2UK0tq" id="6dPxXIRBJ$p" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$n" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJ$k" role="19SJt6">
          <property role="19SUeA" value="subsidie" />
          <node concept="2UK0tq" id="6dPxXIRBJ$l" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJ$j" role="19SJt6">
          <property role="19SUeA" value=" verstrekken voor:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJxO" role="2pmM46">
      <property role="2XObfb" value="Artikel 7 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJxP" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJxQ" role="19SJt6">
          <property role="19SUeA" value="Een subsidie wordt in ieder geval " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJz$" role="19SJt6">
          <property role="19SUeA" value="geweigerd" />
          <node concept="2UK0tq" id="6dPxXIRBJz_" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJzz" role="19SJt6">
          <property role="19SUeA" value=" indien:" />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJyD" role="3H36mW">
      <ref role="1FQA6$" node="7614aO2F_oB" resolve="gedeputeerde staten" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJzO" role="3H36l7">
      <ref role="1FQA6$" node="6dPxXIRBKp2" resolve="aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJ$2" role="3H36lm">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBKps" role="mu1cf">
      <ref role="1FQA6$" node="6dPxXIRBKpp" resolve="weigeren aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBKq3" role="mu1c7">
      <ref role="1FQA6$" node="6dPxXIRBKp2" resolve="aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1zEXH2" id="6dPxXIRBKrZ" role="mu3T0">
      <node concept="1FQA6B" id="6dPxXIRBKsb" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBKs9" resolve="activiteit is al geheel of deels verricht voordat de aanvraag is ingediend" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBKuA" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBKu$" resolve="activiteit is geheel of deels gericht op het behalen van winst" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBKwR" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBKwP" resolve="een andere provinciale subsidie is of kan worden verstrekt voor de activiteit" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBKzx" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBKzv" resolve="activiteit leidt tot investeringen die ongedekte toekomstige lasten tot gevolg hebben" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBKB4" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBKB2" resolve="activiteit bestaat geheel of deels uit een bestaande periodiek terugkerende activiteit" />
      </node>
      <node concept="1zEXH5" id="6dPxXIRBKGI" role="1zF96y">
        <node concept="1FQA6B" id="6dPxXIRBKGZ" role="1zF96A">
          <ref role="1FQA6$" node="6dPxXIRBKE4" resolve="activiteit is in financiële, organisatorische of technische zin haalbaar" />
        </node>
      </node>
      <node concept="1zEXH5" id="6dPxXIRBKHh" role="1zF96y">
        <node concept="1FQA6B" id="6dPxXIRBKHz" role="1zF96A">
          <ref role="1FQA6$" node="6dPxXIRBKHx" resolve="activiteiten zullen leiden tot een betaling van werkelijk gemaakte kosten" />
        </node>
      </node>
      <node concept="1zEWgd" id="6dPxXIRBKLo" role="1zF96y">
        <node concept="1FQA6B" id="6dPxXIRBKLI" role="1zF96y">
          <ref role="1FQA6$" node="6dPxXIRBKLG" resolve="grote maatschappelijke initiatieven" />
        </node>
        <node concept="1zEXH5" id="6dPxXIRBKLT" role="1zF96y">
          <node concept="1FQA6B" id="6dPxXIRBKM5" role="1zF96A">
            <ref role="1FQA6$" node="6dPxXIRBKM3" resolve="een nieuw evenement" />
          </node>
        </node>
        <node concept="1zEXH5" id="6dPxXIRBKMi" role="1zF96y">
          <node concept="1FQA6B" id="6dPxXIRBKMw" role="1zF96A">
            <ref role="1FQA6$" node="6dPxXIRBKMu" resolve="gemeentelijke cofinanciering door één of meer gemeenten" />
          </node>
        </node>
      </node>
      <node concept="1FQA6B" id="6dPxXIRBKSR" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBKPf" resolve="subsidieaanvraag heeft na rangschikking minder dan 10 punten" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBKT_" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBKTz" resolve="aanvraag betrekking heeft op activiteiten die worden uitgesloten van subsidie" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBKVR" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBKVP" resolve="activiteit bestaat geheel of gedeeltelijk uit het opstellen van een dorps- stads- of wijkvisie" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="7614aO2F_qo">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="geweigerd" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="7614aO2F_qp" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="weigeren" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKkB" role="2pmM46">
      <property role="2XObfb" value="Artikel 7 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKkC" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKkD" role="19SJt6">
          <property role="19SUeA" value="Een subsidie wordt in ieder geval " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKkI" role="19SJt6">
          <property role="19SUeA" value="geweigerd" />
          <node concept="2UK0tq" id="6dPxXIRBKkJ" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
          <node concept="2UK0tq" id="6dPxXIRBKl8" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKkK" role="19SJt6">
          <property role="19SUeA" value=" indien:" />
        </node>
      </node>
    </node>
  </node>
  <node concept="mu5$5" id="7614aO2F_qy">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="toekennen punten aan voor subsidieverlening in aanmerking komende aanvraag door gedeputeerde staten" />
    <node concept="1GVOM6" id="7614aO2F_qz" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="toekennen punten aan voor subsidieverlening in aanmerking komende aanvraag door gedeputeerde staten" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_rd" role="3H36mW">
      <ref role="1FQA6$" node="7614aO2F_oB" resolve="gedeputeerde staten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJqH" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJqI" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBJsw" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBJsx" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJsv" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJst" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBJsu" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJss" role="19SJt6">
          <property role="19SUeA" value=" rangschikken de voor subsidieverlening in aanmerking komende aanvragen per  zodanig dat een aanvraag hoger gerangschikt wordt naarmate die naar hun oordeel meer voldoet aan de volgende criteria:" />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJtw" role="3H36lm">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1zEWgd" id="6dPxXIRBL2P" role="mu3T0">
      <node concept="1FQA6B" id="6dPxXIRBL2U" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL3a" resolve="aanvraag heeft betrekking op een gebied" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLb9" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLb7" resolve="toekennen punten voor de mate waarin er draagvlak voor de activiteit bestaat in het betrokken gebied (draagvlak)" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLdR" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLdP" resolve="toekennen punten voor de mate waarin de activiteit leidt tot een voortdurende situatie of een voortdurende activiteit (continuïteit)" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLfS" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLfQ" resolve="toekenning van punten voor de mate waarin de activiteit samenwerking bewerkstelligt in de uitvoering van het project (samenwerking)" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLi7" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLi5" resolve="toekenning van punten voor de mate waarin de activiteit een positieve bijdrage levert aan kansen voor mensen die een minder kansrijke positie in de samenleving hebben (empowerment)" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLoz" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLox" resolve="toekenning van punten voor de mate waarin de mate waarin de activiteit een positieve bijdrage levert aan natuur en milieu (ecology)" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLp0" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLoY" resolve="toekenning van punten voor de mate waarin de mate waarin de activiteit een positieve bijdrage levert aan het zichtbaarheid van het Frysk en/of de Friese streektalen" />
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVys3" role="mu1cf">
      <ref role="1FQA6$" node="6OGuHoEVys0" resolve="aantal punten toegekend aan aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVytg" role="3FTnq6">
      <ref role="1FQA6$" node="6OGuHoEVyte" resolve="toegekend" />
    </node>
    <node concept="cog_b" id="6OGuHoEVytu" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 2 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVytv" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVytw" role="19SJt6">
          <property role="19SUeA" value="Ten behoeve van de rangschikking worden maximaal 27 " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyvM" role="19SJt6">
          <property role="19SUeA" value="punten" />
          <node concept="2UK0tq" id="6OGuHoEVyvN" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyvL" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyuI" role="19SJt6">
          <property role="19SUeA" value="toegekend" />
          <node concept="2UK0tq" id="6OGuHoEVyuJ" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyuH" role="19SJt6">
          <property role="19SUeA" value=" met de volgende maxima per criterium:" />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVywa" role="3H36l7">
      <ref role="1FQA6$" node="6OGuHoEVyw8" resolve="punten" />
    </node>
  </node>
  <node concept="mu5$5" id="7614aO2F_qP">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="vaststellen van onderlinge rangschikking van aanvragen door middel van loting" />
    <property role="3ANC2_" value="Is provinciale staten hier de recipient, en zo ja waarom?" />
    <node concept="1GVOM6" id="7614aO2F_qQ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="vaststellen van onderlinge rangschikking van aanvragen door middel van loting" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_r0" role="3FTnq6">
      <ref role="1FQA6$" node="7614aO2F_qY" resolve="onderlinge rangschikking vaststellen door middel van loting" />
    </node>
    <node concept="1FQA6B" id="7614aO2F_r8" role="3H36mW">
      <ref role="1FQA6$" node="7614aO2F_oB" resolve="gedeputeerde staten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJhc" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 6 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJhd" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJhe" role="19SJt6">
          <property role="19SUeA" value="Voor zover door verstrekking van subsidie voor aanvragen, die even hoog zijn gerangschikt, het subsidieplafond wordt overschreden, wordt de onderlinge rangschikking van die aanvragen vastgesteld door middel van loting." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJkK" role="3H36l7">
      <ref role="1FQA6$" node="6dPxXIRBJkI" resolve="aanvragen die even hoog zijn gerangschikt" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJkw" role="3H36lm">
      <ref role="1FQA6$" node="6dPxXIRBJGa" resolve="provinciale staten" />
    </node>
  </node>
  <node concept="cu0$f" id="7614aO2F_qY">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="onderlinge rangschikking vaststellen door middel van loting" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="7614aO2F_qZ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="onderlinge rangschikking vaststellen door middel van loting" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJhr" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 6 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJhs" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJht" role="19SJt6">
          <property role="19SUeA" value="Voor zover door verstrekking van subsidie voor aanvragen, die even hoog zijn gerangschikt, het subsidieplafond wordt overschreden, wordt de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKYD" role="19SJt6">
          <property role="19SUeA" value="onderlinge" />
          <node concept="2UK0tq" id="6dPxXIRBKYE" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKYC" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJmi" role="19SJt6">
          <property role="19SUeA" value="rangschikking" />
          <node concept="2UK0tq" id="6dPxXIRBJmj" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJmh" role="19SJt6">
          <property role="19SUeA" value=" van die aanvragen " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJif" role="19SJt6">
          <property role="19SUeA" value="vastgesteld" />
          <node concept="2UK0tq" id="6dPxXIRBJig" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJie" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJle" role="19SJt6">
          <property role="19SUeA" value="door" />
          <node concept="2UK0tq" id="6dPxXIRBJlf" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJld" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJla" role="19SJt6">
          <property role="19SUeA" value="middel" />
          <node concept="2UK0tq" id="6dPxXIRBJlb" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJl9" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJl6" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBJl7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJl5" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJl2" role="19SJt6">
          <property role="19SUeA" value="loting." />
          <node concept="2UK0tq" id="6dPxXIRBJl3" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJl1" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJkI">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvragen die even hoog zijn gerangschikt" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJkJ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvragen die even hoog zijn gerangschikt" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJn9" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 6 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJna" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJnb" role="19SJt6">
          <property role="19SUeA" value="Voor zover door verstrekking van subsidie voor aanvragen, die even hoog zijn gerangschikt, het subsidieplafond wordt overschreden, wordt de onderlinge " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJn6" role="19SJt6">
          <property role="19SUeA" value="rangschikking" />
          <node concept="2UK0tq" id="6dPxXIRBJn7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJn5" role="19SJt6">
          <property role="19SUeA" value=" van die aanvragen " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJn3" role="19SJt6">
          <property role="19SUeA" value="vastgesteld" />
          <node concept="2UK0tq" id="6dPxXIRBJn4" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJn2" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJn0" role="19SJt6">
          <property role="19SUeA" value="door" />
          <node concept="2UK0tq" id="6dPxXIRBJn1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJmZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJmX" role="19SJt6">
          <property role="19SUeA" value="middel" />
          <node concept="2UK0tq" id="6dPxXIRBJmY" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJmW" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJmU" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBJmV" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJmT" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJmR" role="19SJt6">
          <property role="19SUeA" value="loting." />
          <node concept="2UK0tq" id="6dPxXIRBJmS" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJnu" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJrs">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="voor subsidieverlening in aanmerking komende aanvragen per gebied" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJrt" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="voor subsidieverlening in aanmerking komende aanvragen per gebied" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJru" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJrv" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJrw" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten rangschikken de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJLC" role="19SJt6">
          <property role="19SUeA" value="voor" />
          <node concept="2UK0tq" id="6dPxXIRBJLD" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJLB" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJL$" role="19SJt6">
          <property role="19SUeA" value="subsidieverlening" />
          <node concept="2UK0tq" id="6dPxXIRBJL_" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJLz" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJLw" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBJLx" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJLv" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJLs" role="19SJt6">
          <property role="19SUeA" value="aanmerking" />
          <node concept="2UK0tq" id="6dPxXIRBJLt" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJLr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJLo" role="19SJt6">
          <property role="19SUeA" value="komende" />
          <node concept="2UK0tq" id="6dPxXIRBJLp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJLn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJLk" role="19SJt6">
          <property role="19SUeA" value="aanvragen" />
          <node concept="2UK0tq" id="6dPxXIRBJLl" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJLj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJLg" role="19SJt6">
          <property role="19SUeA" value="per" />
          <node concept="2UK0tq" id="6dPxXIRBJLh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJLf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJLc" role="19SJt6">
          <property role="19SUeA" value="gebied" />
          <node concept="2UK0tq" id="6dPxXIRBJLd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJLb" role="19SJt6">
          <property role="19SUeA" value=" zodanig dat een aanvraag hoger gerangschikt wordt naarmate die naar hun oordeel meer voldoet aan de volgende criteria:" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJvj">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJvk" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJvl" role="2pmM46">
      <property role="2XObfb" value="Artikel 2 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJvm" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJvY" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten kunnen ter bevordering van de leefbaarheid in de provincie Fryslân subsidie verstrekken voor:" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJAn">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvraag is ingediend binnen de in het openstellingsbesluit vastgestelde aanvraagperiode" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJAo" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvraag is ingediend binnen de in het openstellingsbesluit vastgestelde aanvraagperiode" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJPj" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJPk" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJPl" role="19SJt6">
          <property role="19SUeA" value="Een " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJQ9" role="19SJt6">
          <property role="19SUeA" value="aanvraag" />
          <node concept="2UK0tq" id="6dPxXIRBJQa" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJQ8" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJQ5" role="19SJt6">
          <property role="19SUeA" value="wordt" />
          <node concept="2UK0tq" id="6dPxXIRBJQ6" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJQ4" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJQ1" role="19SJt6">
          <property role="19SUeA" value="ingediend" />
          <node concept="2UK0tq" id="6dPxXIRBJQ2" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJQ0" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJPX" role="19SJt6">
          <property role="19SUeA" value="binnen" />
          <node concept="2UK0tq" id="6dPxXIRBJPY" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJPW" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJPT" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBJPU" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJPS" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJPP" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBJPQ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJPO" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJPL" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6dPxXIRBJPM" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJPK" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJPH" role="19SJt6">
          <property role="19SUeA" value="openstellingsbesluit" />
          <node concept="2UK0tq" id="6dPxXIRBJPI" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJPG" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJPD" role="19SJt6">
          <property role="19SUeA" value="vastgestelde" />
          <node concept="2UK0tq" id="6dPxXIRBJPE" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJPC" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJP_" role="19SJt6">
          <property role="19SUeA" value="aanvraagperiode." />
          <node concept="2UK0tq" id="6dPxXIRBJPA" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJP$" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJAw">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvraag is ingediend met het door gedeputeerde staten vastgestelde aanvraagformulier" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJAx" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvraag is ingediend met het door gedeputeerde staten vastgestelde aanvraagformulier" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJQT" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 2 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJQU" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJQV" role="19SJt6">
          <property role="19SUeA" value="De " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRK" role="19SJt6">
          <property role="19SUeA" value="aanvraag" />
          <node concept="2UK0tq" id="6dPxXIRBJRL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRG" role="19SJt6">
          <property role="19SUeA" value="is" />
          <node concept="2UK0tq" id="6dPxXIRBJRH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRC" role="19SJt6">
          <property role="19SUeA" value="ingediend" />
          <node concept="2UK0tq" id="6dPxXIRBJRD" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRB" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJR$" role="19SJt6">
          <property role="19SUeA" value="met" />
          <node concept="2UK0tq" id="6dPxXIRBJR_" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRz" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRw" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6dPxXIRBJRx" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRv" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRs" role="19SJt6">
          <property role="19SUeA" value="door" />
          <node concept="2UK0tq" id="6dPxXIRBJRt" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRo" role="19SJt6">
          <property role="19SUeA" value="gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBJRp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRk" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBJRl" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRg" role="19SJt6">
          <property role="19SUeA" value="vastgestelde" />
          <node concept="2UK0tq" id="6dPxXIRBJRh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJRc" role="19SJt6">
          <property role="19SUeA" value="aanvraagformulier." />
          <node concept="2UK0tq" id="6dPxXIRBJRd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJRb" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJAD">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="van aanvrager zijn per kalenderjaar maximaal drie aanvragen in behandeling genomen" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJAE" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="van aanvrager zijn per kalenderjaar maximaal drie aanvragen in behandeling genomen" />
    </node>
    <node concept="cog_b" id="6dPxXIRBK5i" role="2pmM46">
      <property role="2XObfb" value="Artikel 6 lid 3 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBK5j" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBK6e" role="19SJt6">
          <property role="19SUeA" value="Per" />
          <node concept="2UK0tq" id="6dPxXIRBK6f" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK6d" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK6b" role="19SJt6">
          <property role="19SUeA" value="aanvrager" />
          <node concept="2UK0tq" id="6dPxXIRBK6c" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK6a" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK67" role="19SJt6">
          <property role="19SUeA" value="worden" />
          <node concept="2UK0tq" id="6dPxXIRBK68" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK66" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK63" role="19SJt6">
          <property role="19SUeA" value="per" />
          <node concept="2UK0tq" id="6dPxXIRBK64" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK62" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK5Z" role="19SJt6">
          <property role="19SUeA" value="kalenderjaar" />
          <node concept="2UK0tq" id="6dPxXIRBK60" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK5Y" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK5V" role="19SJt6">
          <property role="19SUeA" value="maximaal" />
          <node concept="2UK0tq" id="6dPxXIRBK5W" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK5U" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK5R" role="19SJt6">
          <property role="19SUeA" value="drie" />
          <node concept="2UK0tq" id="6dPxXIRBK5S" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK5Q" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK5N" role="19SJt6">
          <property role="19SUeA" value="aanvragen" />
          <node concept="2UK0tq" id="6dPxXIRBK5O" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK5M" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK5J" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBK5K" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK5I" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK5F" role="19SJt6">
          <property role="19SUeA" value="behandeling" />
          <node concept="2UK0tq" id="6dPxXIRBK5G" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK5E" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK5B" role="19SJt6">
          <property role="19SUeA" value="genomen." />
          <node concept="2UK0tq" id="6dPxXIRBK5C" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK5A" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="mu5$5" id="6dPxXIRBJAM">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="vaststellen openstellingsbesluit per gebied door gedeputeerde staten" />
    <node concept="1GVOM6" id="6dPxXIRBJAN" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="vaststellen openstellingsbesluit per gebied door gedeputeerde staten" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJB3" role="3H36mW">
      <ref role="1FQA6$" node="7614aO2F_oB" resolve="gedeputeerde staten" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJBu" role="3FTnq6">
      <ref role="1FQA6$" node="6dPxXIRBJBv" resolve="vaststellen" />
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJDw" role="3H36l7">
      <ref role="1FQA6$" node="6dPxXIRBJDx" resolve="openstellingsbesluit per gebied" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJFu" role="2pmM46">
      <property role="2XObfb" value="Artikel 7 Provinciewet" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJFv" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBJG7" role="19SJt6">
          <property role="19SUeA" value="Provinciale" />
          <node concept="2UK0tq" id="6dPxXIRBJG8" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBx/Recipient" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJG6" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJG4" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBJG5" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBx/Recipient" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJG3" role="19SJt6">
          <property role="19SUeA" value=" vertegenwoordigen de gehele bevolking van de provincie." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6dPxXIRBJG9" role="3H36lm">
      <ref role="1FQA6$" node="6dPxXIRBJGa" resolve="provinciale staten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJHo" role="2pmM46">
      <property role="2XObfb" value="Artikel 2, aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJHp" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJHq" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten kunnen ter bevordering van de leefbaarheid in de provincie Fryslân subsidie verstrekken voor:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBJI0" role="2pmM46">
      <property role="2XObfb" value="Artikel 5 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJI1" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJI2" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJIH" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde" />
          <node concept="2UK0tq" id="6dPxXIRBJII" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJIJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJIK" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBJIL" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJIM" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJIN" role="19SJt6">
          <property role="19SUeA" value="stellen" />
          <node concept="2UK0tq" id="6dPxXIRBJIO" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJIP" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJIQ" role="19SJt6">
          <property role="19SUeA" value="per" />
          <node concept="2UK0tq" id="6dPxXIRBJIR" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJIS" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJIT" role="19SJt6">
          <property role="19SUeA" value="gebied" />
          <node concept="2UK0tq" id="6dPxXIRBJIU" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJIV" role="19SJt6">
          <property role="19SUeA" value=" als bedoeld in artikel 4 een " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJIW" role="19SJt6">
          <property role="19SUeA" value="openstellingsbesluit" />
          <node concept="2UK0tq" id="6dPxXIRBJIX" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJIY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJIZ" role="19SJt6">
          <property role="19SUeA" value="vast." />
          <node concept="2UK0tq" id="6dPxXIRBJJ0" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJJ1" role="19SJt6" />
      </node>
    </node>
    <node concept="1FQA6B" id="6dPxXIRBKry" role="mu1cf">
      <ref role="1FQA6$" node="6dPxXIRBKrv" resolve="openstellingsbesluit per gebied door gedeputeerde staten" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJBv">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="vaststellen" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJBw" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="vaststellen" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJBx" role="2pmM46">
      <property role="2XObfb" value="Artikel 5 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJBy" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJBG" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJCA" role="19SJt6">
          <property role="19SUeA" value="stellen" />
          <node concept="2UK0tq" id="6dPxXIRBJCB" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJC_" role="19SJt6">
          <property role="19SUeA" value=" per gebied als bedoeld in artikel 4 een openstellingsbesluit " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJCP" role="19SJt6">
          <property role="19SUeA" value="vast" />
          <node concept="2UK0tq" id="6dPxXIRBJCQ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJCO" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJDx">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="openstellingsbesluit per gebied" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJDy" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="openstellingsbesluit per gebied" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJDz" role="2pmM46">
      <property role="2XObfb" value="Artikel 5 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJD$" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBJDO" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten stellen " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJF9" role="19SJt6">
          <property role="19SUeA" value="per" />
          <node concept="2UK0tq" id="6dPxXIRBJFa" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJF8" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJF5" role="19SJt6">
          <property role="19SUeA" value="gebied" />
          <node concept="2UK0tq" id="6dPxXIRBJF6" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJF4" role="19SJt6">
          <property role="19SUeA" value=" als bedoeld in artikel 4 een " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBJER" role="19SJt6">
          <property role="19SUeA" value="openstellingsbesluit" />
          <node concept="2UK0tq" id="6dPxXIRBJES" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBJEQ" role="19SJt6">
          <property role="19SUeA" value=" vast" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBJGa">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="provinciale staten" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBJGb" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="provinciale staten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBJGE" role="2pmM46">
      <property role="2XObfb" value="Artikel 7 Provinciewet" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBJGF" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBK4J" role="19SJt6">
          <property role="19SUeA" value="Provinciale" />
          <node concept="2UK0tq" id="6dPxXIRBK4K" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK4I" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK4G" role="19SJt6">
          <property role="19SUeA" value="staten" />
          <node concept="2UK0tq" id="6dPxXIRBK4H" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK4F" role="19SJt6">
          <property role="19SUeA" value=" vertegenwoordigen de gehele bevolking van de provincie." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKp2">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1RnfdX" id="6dPxXIRBKp3" role="coNO9" />
    <node concept="1GVOM6" id="6dPxXIRBKp4" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKpp">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="weigeren aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1RnfdX" id="6dPxXIRBKpq" role="coNO9" />
    <node concept="1GVOM6" id="6dPxXIRBKpr" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="besluit tot weigeren aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKpE">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="besluit tot verstrekken subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1RnfdX" id="6dPxXIRBKpF" role="coNO9" />
    <node concept="1GVOM6" id="6dPxXIRBKpG" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="besluit tot verstrekken subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKrv">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="openstellingsbesluit per gebied door gedeputeerde staten" />
    <property role="3GE5qa" value="facts" />
    <node concept="1RnfdX" id="6dPxXIRBKrw" role="coNO9" />
    <node concept="1GVOM6" id="6dPxXIRBKrx" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="openstellingsbesluit per gebied door gedeputeerde staten" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKs9">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit is al geheel of deels verricht voordat de aanvraag is ingediend" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKsa" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit is al geheel of deels verricht voordat de aanvraag is ingediend" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKsm" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel a Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKsn" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBKtM" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBKtN" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKtL" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtJ" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBKtK" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKtI" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtF" role="19SJt6">
          <property role="19SUeA" value="al" />
          <node concept="2UK0tq" id="6dPxXIRBKtG" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKtE" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtB" role="19SJt6">
          <property role="19SUeA" value="geheel" />
          <node concept="2UK0tq" id="6dPxXIRBKtC" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKtA" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtz" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6dPxXIRBKt$" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKty" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtv" role="19SJt6">
          <property role="19SUeA" value="deels" />
          <node concept="2UK0tq" id="6dPxXIRBKtw" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKtu" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtr" role="19SJt6">
          <property role="19SUeA" value="is" />
          <node concept="2UK0tq" id="6dPxXIRBKts" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKtq" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtn" role="19SJt6">
          <property role="19SUeA" value="verricht" />
          <node concept="2UK0tq" id="6dPxXIRBKto" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKtm" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtj" role="19SJt6">
          <property role="19SUeA" value="voordat" />
          <node concept="2UK0tq" id="6dPxXIRBKtk" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKti" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtf" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBKtg" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKte" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKtb" role="19SJt6">
          <property role="19SUeA" value="aanvraag" />
          <node concept="2UK0tq" id="6dPxXIRBKtc" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKta" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKt7" role="19SJt6">
          <property role="19SUeA" value="is" />
          <node concept="2UK0tq" id="6dPxXIRBKt8" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKt6" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKt3" role="19SJt6">
          <property role="19SUeA" value="ingediend" />
          <node concept="2UK0tq" id="6dPxXIRBKt4" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKt2" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKu$">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit is geheel of deels gericht op het behalen van winst" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKu_" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit is geheel of deels gericht op het behalen van winst" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKuJ" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel b Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKuK" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKuL" role="19SJt6">
          <property role="19SUeA" value="de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKvG" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBKvH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKvC" role="19SJt6">
          <property role="19SUeA" value="geheel" />
          <node concept="2UK0tq" id="6dPxXIRBKvD" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvB" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKv$" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6dPxXIRBKv_" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvz" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKvw" role="19SJt6">
          <property role="19SUeA" value="deels" />
          <node concept="2UK0tq" id="6dPxXIRBKvx" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvv" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKvs" role="19SJt6">
          <property role="19SUeA" value="gericht" />
          <node concept="2UK0tq" id="6dPxXIRBKvt" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKvo" role="19SJt6">
          <property role="19SUeA" value="is" />
          <node concept="2UK0tq" id="6dPxXIRBKvp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKvk" role="19SJt6">
          <property role="19SUeA" value="op" />
          <node concept="2UK0tq" id="6dPxXIRBKvl" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKvg" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6dPxXIRBKvh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKvc" role="19SJt6">
          <property role="19SUeA" value="behalen" />
          <node concept="2UK0tq" id="6dPxXIRBKvd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKvb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKv8" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBKv9" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKv7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKv4" role="19SJt6">
          <property role="19SUeA" value="winst;" />
          <node concept="2UK0tq" id="6dPxXIRBKv5" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKv3" role="19SJt6">
          <property role="19SUeA" value=" met uitzondering van die activiteiten die economische van aard zijn en door een kleine of micro-onderneming zijn ingediend, wanneer de te verlenen subsidie niet de drempelbedragen genoemd uit de de-minimisverordeningen zoals opgenomen onder artikel 1, onder e en f overschrijden;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKwP">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="een andere provinciale subsidie is of kan worden verstrekt voor de activiteit" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKwQ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="een andere provinciale subsidie is of kan worden verstrekt voor de activiteit" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKx1" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel c Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKx2" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBKy1" role="19SJt6">
          <property role="19SUeA" value="voor" />
          <node concept="2UK0tq" id="6dPxXIRBKy2" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKy0" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxY" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBKxZ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxX" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxU" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBKxV" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxT" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxQ" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBKxR" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxP" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxM" role="19SJt6">
          <property role="19SUeA" value="andere" />
          <node concept="2UK0tq" id="6dPxXIRBKxN" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxL" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxI" role="19SJt6">
          <property role="19SUeA" value="provinciale" />
          <node concept="2UK0tq" id="6dPxXIRBKxJ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxH" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxE" role="19SJt6">
          <property role="19SUeA" value="subsidie" />
          <node concept="2UK0tq" id="6dPxXIRBKxF" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxD" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxA" role="19SJt6">
          <property role="19SUeA" value="is" />
          <node concept="2UK0tq" id="6dPxXIRBKxB" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKx_" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxy" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6dPxXIRBKxz" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxx" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxu" role="19SJt6">
          <property role="19SUeA" value="kan" />
          <node concept="2UK0tq" id="6dPxXIRBKxv" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxt" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxq" role="19SJt6">
          <property role="19SUeA" value="worden" />
          <node concept="2UK0tq" id="6dPxXIRBKxr" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxp" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKxm" role="19SJt6">
          <property role="19SUeA" value="verstrekt;" />
          <node concept="2UK0tq" id="6dPxXIRBKxn" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKxl" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKzv">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit leidt tot investeringen die ongedekte toekomstige lasten tot gevolg hebben" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKzw" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit leidt tot investeringen die ongedekte toekomstige lasten tot gevolg hebben" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKzG" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel d Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKzH" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKzI" role="19SJt6">
          <property role="19SUeA" value="het aannemelijk is dat de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$T" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBK$U" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$S" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$P" role="19SJt6">
          <property role="19SUeA" value="leidt" />
          <node concept="2UK0tq" id="6dPxXIRBK$Q" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$O" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$L" role="19SJt6">
          <property role="19SUeA" value="tot" />
          <node concept="2UK0tq" id="6dPxXIRBK$M" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$K" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$H" role="19SJt6">
          <property role="19SUeA" value="investeringen" />
          <node concept="2UK0tq" id="6dPxXIRBK$I" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$G" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$D" role="19SJt6">
          <property role="19SUeA" value="die" />
          <node concept="2UK0tq" id="6dPxXIRBK$E" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$C" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$_" role="19SJt6">
          <property role="19SUeA" value="ongedekte" />
          <node concept="2UK0tq" id="6dPxXIRBK$A" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$$" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$x" role="19SJt6">
          <property role="19SUeA" value="toekomstige" />
          <node concept="2UK0tq" id="6dPxXIRBK$y" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$w" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$t" role="19SJt6">
          <property role="19SUeA" value="lasten" />
          <node concept="2UK0tq" id="6dPxXIRBK$u" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$s" role="19SJt6">
          <property role="19SUeA" value=" voor&#10;onderhoud of instandhouding " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$9" role="19SJt6">
          <property role="19SUeA" value="tot" />
          <node concept="2UK0tq" id="6dPxXIRBK$a" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$8" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$5" role="19SJt6">
          <property role="19SUeA" value="gevolg" />
          <node concept="2UK0tq" id="6dPxXIRBK$6" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$4" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBK$1" role="19SJt6">
          <property role="19SUeA" value="hebben;" />
          <node concept="2UK0tq" id="6dPxXIRBK$2" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBK$0" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKB2">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit bestaat geheel of deels uit een bestaande periodiek terugkerende activiteit" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKB3" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit bestaat geheel of deels uit een bestaande periodiek terugkerende activiteit" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKBg" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel e Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKBh" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKCT" role="19SJt6">
          <property role="19SUeA" value="de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKCd" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBKCe" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKCc" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKC9" role="19SJt6">
          <property role="19SUeA" value="geheel" />
          <node concept="2UK0tq" id="6dPxXIRBKCa" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKC8" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKC5" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6dPxXIRBKC6" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKC4" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKC1" role="19SJt6">
          <property role="19SUeA" value="deels" />
          <node concept="2UK0tq" id="6dPxXIRBKC2" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKC0" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKBX" role="19SJt6">
          <property role="19SUeA" value="bestaat" />
          <node concept="2UK0tq" id="6dPxXIRBKBY" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKBW" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKBT" role="19SJt6">
          <property role="19SUeA" value="uit" />
          <node concept="2UK0tq" id="6dPxXIRBKBU" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKBS" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKBP" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBKBQ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKBO" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKBL" role="19SJt6">
          <property role="19SUeA" value="bestaande" />
          <node concept="2UK0tq" id="6dPxXIRBKBM" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKBK" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKBH" role="19SJt6">
          <property role="19SUeA" value="periodiek" />
          <node concept="2UK0tq" id="6dPxXIRBKBI" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKBG" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKBD" role="19SJt6">
          <property role="19SUeA" value="terugkerende" />
          <node concept="2UK0tq" id="6dPxXIRBKBE" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKBC" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKB_" role="19SJt6">
          <property role="19SUeA" value="activiteit;" />
          <node concept="2UK0tq" id="6dPxXIRBKBA" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKB$" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKE4">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit is in financiële, organisatorische of technische zin haalbaar" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKE5" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit is in financiële, organisatorische of technische zin haalbaar" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKEj" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel f Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKEk" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKEl" role="19SJt6">
          <property role="19SUeA" value="er gegronde reden bestaat dat de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKFc" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBKFd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKFb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKF8" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBKF9" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKF7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKF4" role="19SJt6">
          <property role="19SUeA" value="financiële," />
          <node concept="2UK0tq" id="6dPxXIRBKF5" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKF3" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKF0" role="19SJt6">
          <property role="19SUeA" value="organisatorische" />
          <node concept="2UK0tq" id="6dPxXIRBKF1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKEZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKEW" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6dPxXIRBKEX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKEV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKES" role="19SJt6">
          <property role="19SUeA" value="technische" />
          <node concept="2UK0tq" id="6dPxXIRBKET" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKER" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKEO" role="19SJt6">
          <property role="19SUeA" value="zin" />
          <node concept="2UK0tq" id="6dPxXIRBKEP" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKEN" role="19SJt6">
          <property role="19SUeA" value=" niet " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKEG" role="19SJt6">
          <property role="19SUeA" value="haalbaar" />
          <node concept="2UK0tq" id="6dPxXIRBKEH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKEF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKEC" role="19SJt6">
          <property role="19SUeA" value="is;" />
          <node concept="2UK0tq" id="6dPxXIRBKED" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKEB" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1zEWgd" id="6OGuHoEVz3b" role="coNO9">
      <node concept="1FQA6B" id="6OGuHoEVz3q" role="1zF96y">
        <ref role="1FQA6$" node="6OGuHoEVyZs" resolve="activiteit in financiële zin haalbaar" />
      </node>
      <node concept="1FQA6B" id="6OGuHoEVz3y" role="1zF96y">
        <ref role="1FQA6$" node="6OGuHoEVz18" resolve="activiteit in is organisatorische zin haalbaar" />
      </node>
      <node concept="1FQA6B" id="6OGuHoEVz3F" role="1zF96y">
        <ref role="1FQA6$" node="6OGuHoEVyYQ" resolve="activiteit is in technische zin haalbaar" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKHx">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteiten zullen leiden tot een betaling van werkelijk gemaakte kosten" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKHy" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteiten zullen leiden tot een betaling van werkelijk gemaakte kosten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKHC" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel g Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKHD" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKHE" role="19SJt6">
          <property role="19SUeA" value="de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKIy" role="19SJt6">
          <property role="19SUeA" value="activiteiten" />
          <node concept="2UK0tq" id="6dPxXIRBKIz" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKIx" role="19SJt6">
          <property role="19SUeA" value=" niet " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKIq" role="19SJt6">
          <property role="19SUeA" value="zullen" />
          <node concept="2UK0tq" id="6dPxXIRBKIr" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKIp" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKIm" role="19SJt6">
          <property role="19SUeA" value="leiden" />
          <node concept="2UK0tq" id="6dPxXIRBKIn" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKIl" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKIi" role="19SJt6">
          <property role="19SUeA" value="tot" />
          <node concept="2UK0tq" id="6dPxXIRBKIj" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKIh" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKIe" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBKIf" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKId" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKIa" role="19SJt6">
          <property role="19SUeA" value="betaling" />
          <node concept="2UK0tq" id="6dPxXIRBKIb" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKI9" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKI6" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBKI7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKI5" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKI2" role="19SJt6">
          <property role="19SUeA" value="werkelijk" />
          <node concept="2UK0tq" id="6dPxXIRBKI3" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKI1" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKHY" role="19SJt6">
          <property role="19SUeA" value="gemaakte" />
          <node concept="2UK0tq" id="6dPxXIRBKHZ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKHX" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKHU" role="19SJt6">
          <property role="19SUeA" value="kosten;" />
          <node concept="2UK0tq" id="6dPxXIRBKHV" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKHT" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKLG">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="grote maatschappelijke initiatieven" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKLH" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="grote maatschappelijke initiatieven" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKMN" role="2pmM46">
      <property role="2XObfb" value="Artikel 2, onderdeel c Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKMO" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKMP" role="19SJt6">
          <property role="19SUeA" value="grote maatschappelijke initiatieven, niet zijnde een nieuw evenement." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKM3">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="een nieuw evenement" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKM4" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="een nieuw evenement" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKKS" role="2pmM46">
      <property role="2XObfb" value="Artikel 2, onderdeel c Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKKT" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKKU" role="19SJt6">
          <property role="19SUeA" value="grote maatschappelijke initiatieven, niet zijnde een nieuw evenement." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKMu">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="gemeentelijke cofinanciering door één of meer gemeenten" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKMv" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="gemeentelijke cofinanciering door één of meer gemeenten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKNJ" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel h Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKNK" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKNL" role="19SJt6">
          <property role="19SUeA" value="gemeentelijke cofinanciering grote maatschappelijke initiatieven &#10;er onder activiteiten die vallen onder artikel 2, onder c geen sprake is van gemeentelijke cofinanciering door één of meer gemeenten;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKPf">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="subsidieaanvraag heeft na rangschikking minder dan 10 punten" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKPg" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="subsidieaanvraag heeft na rangschikking minder dan 10 punten" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKPm" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel i Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKPn" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKPo" role="19SJt6">
          <property role="19SUeA" value="de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKPK" role="19SJt6">
          <property role="19SUeA" value="subsidieaanvraag" />
          <node concept="2UK0tq" id="6dPxXIRBKPL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKPJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKPG" role="19SJt6">
          <property role="19SUeA" value="na" />
          <node concept="2UK0tq" id="6dPxXIRBKPH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKPF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKPC" role="19SJt6">
          <property role="19SUeA" value="rangschikking" />
          <node concept="2UK0tq" id="6dPxXIRBKPD" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKPB" role="19SJt6">
          <property role="19SUeA" value=" als bedoeld in artikel 8, eerste lid, " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKQq" role="19SJt6">
          <property role="19SUeA" value="minder" />
          <node concept="2UK0tq" id="6dPxXIRBKQr" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKQp" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKQm" role="19SJt6">
          <property role="19SUeA" value="dan" />
          <node concept="2UK0tq" id="6dPxXIRBKQn" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKQl" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKQi" role="19SJt6">
          <property role="19SUeA" value="10" />
          <node concept="2UK0tq" id="6dPxXIRBKQj" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKQh" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKQe" role="19SJt6">
          <property role="19SUeA" value="punten" />
          <node concept="2UK0tq" id="6dPxXIRBKQf" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKQd" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKQa" role="19SJt6">
          <property role="19SUeA" value="heeft" />
          <node concept="2UK0tq" id="6dPxXIRBKQb" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKQ9" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKQ6" role="19SJt6">
          <property role="19SUeA" value="ontvangen;" />
          <node concept="2UK0tq" id="6dPxXIRBKQ7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKQ5" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKTz">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvraag betrekking heeft op activiteiten die worden uitgesloten van subsidie" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKT$" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvraag betrekking heeft op activiteiten die worden uitgesloten van subsidie" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKTU" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel j Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKTV" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKTW" role="19SJt6">
          <property role="19SUeA" value="de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUP" role="19SJt6">
          <property role="19SUeA" value="aanvraag" />
          <node concept="2UK0tq" id="6dPxXIRBKUQ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUO" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUL" role="19SJt6">
          <property role="19SUeA" value="betrekking" />
          <node concept="2UK0tq" id="6dPxXIRBKUM" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUK" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUH" role="19SJt6">
          <property role="19SUeA" value="heeft" />
          <node concept="2UK0tq" id="6dPxXIRBKUI" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUG" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUD" role="19SJt6">
          <property role="19SUeA" value="op" />
          <node concept="2UK0tq" id="6dPxXIRBKUE" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUC" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKU_" role="19SJt6">
          <property role="19SUeA" value="activiteiten" />
          <node concept="2UK0tq" id="6dPxXIRBKUA" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKU$" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUx" role="19SJt6">
          <property role="19SUeA" value="die" />
          <node concept="2UK0tq" id="6dPxXIRBKUy" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUw" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUt" role="19SJt6">
          <property role="19SUeA" value="worden" />
          <node concept="2UK0tq" id="6dPxXIRBKUu" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUs" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUp" role="19SJt6">
          <property role="19SUeA" value="uitgesloten" />
          <node concept="2UK0tq" id="6dPxXIRBKUq" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUo" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUl" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBKUm" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUk" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKUh" role="19SJt6">
          <property role="19SUeA" value="subsidie" />
          <node concept="2UK0tq" id="6dPxXIRBKUi" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKUg" role="19SJt6">
          <property role="19SUeA" value=" op grond van beleid van provinciale staten of gedeputeerde staten;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBKVP">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit bestaat geheel of gedeeltelijk uit het opstellen van een dorps- stads- of wijkvisie" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBKVQ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit bestaat geheel of gedeeltelijk uit het opstellen van een dorps- stads- of wijkvisie" />
    </node>
    <node concept="cog_b" id="6dPxXIRBKWg" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel k Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBKWh" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBKWi" role="19SJt6">
          <property role="19SUeA" value="de " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKXp" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBKXq" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKXo" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKXl" role="19SJt6">
          <property role="19SUeA" value="geheel" />
          <node concept="2UK0tq" id="6dPxXIRBKXm" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKXk" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKXh" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6dPxXIRBKXi" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKXg" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKXd" role="19SJt6">
          <property role="19SUeA" value="gedeeltelijk" />
          <node concept="2UK0tq" id="6dPxXIRBKXe" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKXc" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKX9" role="19SJt6">
          <property role="19SUeA" value="bestaat" />
          <node concept="2UK0tq" id="6dPxXIRBKXa" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKX8" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKX5" role="19SJt6">
          <property role="19SUeA" value="uit" />
          <node concept="2UK0tq" id="6dPxXIRBKX6" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKX4" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKX1" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6dPxXIRBKX2" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKX0" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKWX" role="19SJt6">
          <property role="19SUeA" value="opstellen" />
          <node concept="2UK0tq" id="6dPxXIRBKWY" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKWW" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKWT" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBKWU" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKWS" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKWP" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBKWQ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKWO" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKWL" role="19SJt6">
          <property role="19SUeA" value="dorps-" />
          <node concept="2UK0tq" id="6dPxXIRBKWM" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKWK" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKWH" role="19SJt6">
          <property role="19SUeA" value="stads-" />
          <node concept="2UK0tq" id="6dPxXIRBKWI" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKWG" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKWD" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6dPxXIRBKWE" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKWC" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBKW_" role="19SJt6">
          <property role="19SUeA" value="wijkvisie." />
          <node concept="2UK0tq" id="6dPxXIRBKWA" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBKW$" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL3a">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvraag heeft betrekking op een gebied" />
    <property role="3GE5qa" value="facts" />
    <property role="3ANC2_" value="Het opgeven van een gebied waarop de aanvraag betrekking heeft, is niet expliciet vastgelegd in de regeling. Er is wel vastgelegd dat er een openstellingsbesluit per gebied moet zijn (artikel 5 lid 1), welke gemeenten er in een gebied liggen (artikel 4) en dat er per gebied een rangschikking wordt gemaakt (artikel 8 lid 1) en dat ieder gebied een eigen subsidieplafond heeft (artikel 8 lid 2)." />
    <node concept="1GVOM6" id="6dPxXIRBL3b" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvraag heeft betrekking op een gebied" />
    </node>
    <node concept="cog_b" id="6dPxXIRBL3k" role="2pmM46">
      <property role="2XObfb" value="Artikel 5 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBL3l" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBL3m" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten stellen per gebied als bedoeld in artikel 4 een openstellingsbesluit vast." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBL5S" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBL5T" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBL5U" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten rangschikken de voor subsidieverlening in aanmerking komende aanvragen per gebied zodanig dat een aanvraag hoger gerangschikt wordt naarmate die naar hun oordeel meer voldoet aan de volgende criteria:" />
        </node>
      </node>
    </node>
    <node concept="1zEXH2" id="6dPxXIRBL70" role="coNO9">
      <node concept="1FQA6B" id="6dPxXIRBL7H" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL7F" resolve="Waddeneilanden" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL7S" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL7Q" resolve="Noordoost Fryslân" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL7j" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL7Q" resolve="Noordoost Fryslân" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL8g" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL8e" resolve="Zuidwest Fryslân" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL8s" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL8q" resolve="Zuidoost Fryslân" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL7F">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="Waddeneilanden" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL7G" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="Waddeneilanden" />
    </node>
    <node concept="1zEXH2" id="6dPxXIRBL8L" role="coNO9">
      <node concept="1FQA6B" id="6dPxXIRBLao" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLam" resolve="gemeente Ameland" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLay" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLaw" resolve="gemeente Schiermonnikoog" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLaG" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLaE" resolve="gemeente Terschelling" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLaQ" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLaO" resolve="gemeente Vlieland" />
      </node>
    </node>
    <node concept="cog_b" id="6dPxXIRBLa8" role="2pmM46">
      <property role="2XObfb" value="Artikel 4 onderdeel a Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLa9" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLaa" role="19SJt6">
          <property role="19SUeA" value="Waddeneilanden, bestaande uit de gemeenten: Ameland, Schiermonnikoog, Terschelling en Vlieland;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL7Q">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="Noordoost Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL7R" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="Noordoost Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL8e">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="Zuidwest Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL8f" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="Zuidwest Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL8q">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="Zuidoost Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL8r" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="Zuidoost Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLam">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="gemeente Ameland" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLan" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="gemeente Ameland" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLaw">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="gemeente Schiermonnikoog" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLax" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="gemeente Schiermonnikoog" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLaE">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="gemeente Terschelling" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLaF" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="gemeente Terschelling" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLaO">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="gemeente Vlieland" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLaP" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="gemeente Vlieland" />
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLb7">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="toekennen punten voor de mate waarin er draagvlak voor de activiteit bestaat in het betrokken gebied (draagvlak)" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLb8" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="toekennen punten voor de mate waarin er draagvlak voor de activiteit bestaat in het betrokken gebied (draagvlak)" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLbm" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 onderdeel a Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLbn" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBLPW" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLPX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLPV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLPT" role="19SJt6">
          <property role="19SUeA" value="mate" />
          <node concept="2UK0tq" id="6dPxXIRBLPU" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLPS" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLPP" role="19SJt6">
          <property role="19SUeA" value="waarin" />
          <node concept="2UK0tq" id="6dPxXIRBLPQ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLPO" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLPL" role="19SJt6">
          <property role="19SUeA" value="er" />
          <node concept="2UK0tq" id="6dPxXIRBLPM" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLPK" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLcc" role="19SJt6">
          <property role="19SUeA" value="draagvlak" />
          <node concept="2UK0tq" id="6dPxXIRBLcd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLcb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLc8" role="19SJt6">
          <property role="19SUeA" value="voor" />
          <node concept="2UK0tq" id="6dPxXIRBLc9" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLc7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLc4" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLc5" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLc3" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLc0" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBLc1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLbZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLbW" role="19SJt6">
          <property role="19SUeA" value="bestaat" />
          <node concept="2UK0tq" id="6dPxXIRBLbX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLbV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLbS" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBLbT" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLbR" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLbO" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6dPxXIRBLbP" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLbN" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLbK" role="19SJt6">
          <property role="19SUeA" value="betrokken" />
          <node concept="2UK0tq" id="6dPxXIRBLbL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLbJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLbG" role="19SJt6">
          <property role="19SUeA" value="gebied" />
          <node concept="2UK0tq" id="6dPxXIRBLbH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLbF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLbC" role="19SJt6">
          <property role="19SUeA" value="(draagvlak);" />
          <node concept="2UK0tq" id="6dPxXIRBLbD" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLbB" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1zEXH2" id="6dPxXIRBLtV" role="coNO9">
      <node concept="1FQA6B" id="6dPxXIRBL$k" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$i" resolve="geen punten toegekend indien niet wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL$w" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$u" resolve="één punt toegekend indien er in zeer geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL$G" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$E" resolve="twee punten toegekend indien er in geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL$S" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$Q" resolve="drie punten toegekend indien gemiddeld wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL_4" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_2" resolve="vier punten toegekend indien in ruime mate wordt voldaan aan het criterium " />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL_j" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_h" resolve="vijf punten toegekend indien in zeer ruime mate wordt voldaan aan het criterium" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLdP">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="toekennen punten voor de mate waarin de activiteit leidt tot een voortdurende situatie of een voortdurende activiteit (continuïteit)" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLdQ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="toekennen punten voor de mate waarin de activiteit leidt tot een voortdurende situatie of een voortdurende activiteit (continuïteit)" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLe1" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 onderdeel b Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLe2" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBLJh" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLJi" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLJg" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLJe" role="19SJt6">
          <property role="19SUeA" value="mate" />
          <node concept="2UK0tq" id="6dPxXIRBLJf" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLJd" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLJa" role="19SJt6">
          <property role="19SUeA" value="waarin" />
          <node concept="2UK0tq" id="6dPxXIRBLJb" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLJ9" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLJ6" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLJ7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLJ5" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeY" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBLeZ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLeX" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeU" role="19SJt6">
          <property role="19SUeA" value="leidt" />
          <node concept="2UK0tq" id="6dPxXIRBLeV" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLeT" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeQ" role="19SJt6">
          <property role="19SUeA" value="tot" />
          <node concept="2UK0tq" id="6dPxXIRBLeR" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLeP" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeM" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBLeN" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLeL" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeI" role="19SJt6">
          <property role="19SUeA" value="voortdurende" />
          <node concept="2UK0tq" id="6dPxXIRBLeJ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLeH" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeE" role="19SJt6">
          <property role="19SUeA" value="situatie" />
          <node concept="2UK0tq" id="6dPxXIRBLeF" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLeD" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeA" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6dPxXIRBLeB" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLe_" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLey" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBLez" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLex" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeu" role="19SJt6">
          <property role="19SUeA" value="voortdurende" />
          <node concept="2UK0tq" id="6dPxXIRBLev" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLet" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLeq" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBLer" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLep" role="19SJt6">
          <property role="19SUeA" value="&#10;" />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLem" role="19SJt6">
          <property role="19SUeA" value="(continuïteit);" />
          <node concept="2UK0tq" id="6dPxXIRBLen" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLel" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1zEXH2" id="6dPxXIRBL_z" role="coNO9">
      <node concept="1FQA6B" id="6dPxXIRBL_F" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$i" resolve="geen punten toegekend indien niet wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL_N" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$u" resolve="één punt toegekend indien er in zeer geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBL_W" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$E" resolve="twee punten toegekend indien er in geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLA6" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$Q" resolve="drie punten toegekend indien gemiddeld wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLAh" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_2" resolve="vier punten toegekend indien in ruime mate wordt voldaan aan het criterium " />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLAt" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_h" resolve="vijf punten toegekend indien in zeer ruime mate wordt voldaan aan het criterium" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLfQ">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="toekenning van punten voor de mate waarin de activiteit samenwerking bewerkstelligt in de uitvoering van het project (samenwerking)" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLfR" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="toekenning van punten voor de mate waarin de activiteit samenwerking bewerkstelligt in de uitvoering van het project (samenwerking)" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLg3" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 onderdeel c Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLg4" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBLQZ" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLR0" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLQY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLQW" role="19SJt6">
          <property role="19SUeA" value="mate" />
          <node concept="2UK0tq" id="6dPxXIRBLQX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLQV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLQS" role="19SJt6">
          <property role="19SUeA" value="waarin" />
          <node concept="2UK0tq" id="6dPxXIRBLQT" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLQR" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLh0" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLh1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgW" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBLgX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgS" role="19SJt6">
          <property role="19SUeA" value="samenwerking" />
          <node concept="2UK0tq" id="6dPxXIRBLgT" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgR" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgO" role="19SJt6">
          <property role="19SUeA" value="bewerkstelligt" />
          <node concept="2UK0tq" id="6dPxXIRBLgP" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgN" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgK" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBLgL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgG" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLgH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgC" role="19SJt6">
          <property role="19SUeA" value="uitvoering" />
          <node concept="2UK0tq" id="6dPxXIRBLgD" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgB" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLg$" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBLg_" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgz" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgw" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6dPxXIRBLgx" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgv" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgs" role="19SJt6">
          <property role="19SUeA" value="project" />
          <node concept="2UK0tq" id="6dPxXIRBLgt" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLgo" role="19SJt6">
          <property role="19SUeA" value="(samenwerking);" />
          <node concept="2UK0tq" id="6dPxXIRBLgp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLgn" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1zEXH2" id="6dPxXIRBLAH" role="coNO9">
      <node concept="1FQA6B" id="6dPxXIRBLAP" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$u" resolve="één punt toegekend indien er in zeer geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLAX" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$E" resolve="twee punten toegekend indien er in geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLB6" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$Q" resolve="drie punten toegekend indien gemiddeld wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLBg" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_2" resolve="vier punten toegekend indien in ruime mate wordt voldaan aan het criterium " />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLBr" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_h" resolve="vijf punten toegekend indien in zeer ruime mate wordt voldaan aan het criterium" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLi5">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="toekenning van punten voor de mate waarin de activiteit een positieve bijdrage levert aan kansen voor mensen die een minder kansrijke positie in de samenleving hebben (empowerment)" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLi6" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="toekenning van punten voor de mate waarin de activiteit een positieve bijdrage levert aan kansen voor mensen die een minder kansrijke positie in de samenleving hebben (empowerment)" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLij" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 onderdeel d Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLik" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBLT$" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLT_" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLTz" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLTx" role="19SJt6">
          <property role="19SUeA" value="mate" />
          <node concept="2UK0tq" id="6dPxXIRBLTy" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLTw" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLTt" role="19SJt6">
          <property role="19SUeA" value="waarin" />
          <node concept="2UK0tq" id="6dPxXIRBLTu" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLTs" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLTp" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLTq" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLTo" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmO" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBLmP" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmN" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmK" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBLmL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmG" role="19SJt6">
          <property role="19SUeA" value="positieve" />
          <node concept="2UK0tq" id="6dPxXIRBLmH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmC" role="19SJt6">
          <property role="19SUeA" value="bijdrage" />
          <node concept="2UK0tq" id="6dPxXIRBLmD" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmB" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLm$" role="19SJt6">
          <property role="19SUeA" value="levert" />
          <node concept="2UK0tq" id="6dPxXIRBLm_" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmz" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmw" role="19SJt6">
          <property role="19SUeA" value="aan" />
          <node concept="2UK0tq" id="6dPxXIRBLmx" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmv" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLms" role="19SJt6">
          <property role="19SUeA" value="kansen" />
          <node concept="2UK0tq" id="6dPxXIRBLmt" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmo" role="19SJt6">
          <property role="19SUeA" value="voor" />
          <node concept="2UK0tq" id="6dPxXIRBLmp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmk" role="19SJt6">
          <property role="19SUeA" value="mensen" />
          <node concept="2UK0tq" id="6dPxXIRBLml" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmg" role="19SJt6">
          <property role="19SUeA" value="die" />
          <node concept="2UK0tq" id="6dPxXIRBLmh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLmc" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBLmd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLmb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLm8" role="19SJt6">
          <property role="19SUeA" value="minder" />
          <node concept="2UK0tq" id="6dPxXIRBLm9" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLm7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLm4" role="19SJt6">
          <property role="19SUeA" value="kansrijke" />
          <node concept="2UK0tq" id="6dPxXIRBLm5" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLm3" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLm0" role="19SJt6">
          <property role="19SUeA" value="positie" />
          <node concept="2UK0tq" id="6dPxXIRBLm1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLlZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLlW" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6dPxXIRBLlX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLlV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLlS" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLlT" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLlR" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLlO" role="19SJt6">
          <property role="19SUeA" value="samenleving" />
          <node concept="2UK0tq" id="6dPxXIRBLlP" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLlN" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLlK" role="19SJt6">
          <property role="19SUeA" value="hebben" />
          <node concept="2UK0tq" id="6dPxXIRBLlL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLlJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLlG" role="19SJt6">
          <property role="19SUeA" value="(empowerment);" />
          <node concept="2UK0tq" id="6dPxXIRBLlH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLlF" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1zEXH2" id="6dPxXIRBLBM" role="coNO9">
      <node concept="1FQA6B" id="6dPxXIRBLCm" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$i" resolve="geen punten toegekend indien niet wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLCu" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$u" resolve="één punt toegekend indien er in zeer geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLCB" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$E" resolve="twee punten toegekend indien er in geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLCL" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$Q" resolve="drie punten toegekend indien gemiddeld wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLCW" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_2" resolve="vier punten toegekend indien in ruime mate wordt voldaan aan het criterium " />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLD8" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_h" resolve="vijf punten toegekend indien in zeer ruime mate wordt voldaan aan het criterium" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLox">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="toekenning van punten voor de mate waarin de mate waarin de activiteit een positieve bijdrage levert aan natuur en milieu (ecology)" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLoy" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="toekenning van punten voor de mate waarin de mate waarin de activiteit een positieve bijdrage levert aan natuur en milieu (ecology)" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLpv" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 onderdeel e Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLpw" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBLW6" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLW7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLW5" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLW3" role="19SJt6">
          <property role="19SUeA" value="mate" />
          <node concept="2UK0tq" id="6dPxXIRBLW4" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLW2" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLVZ" role="19SJt6">
          <property role="19SUeA" value="waarin" />
          <node concept="2UK0tq" id="6dPxXIRBLW0" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLVY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLqs" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLqt" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLqr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLqo" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBLqp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLqn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLqk" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBLql" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLqj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLqg" role="19SJt6">
          <property role="19SUeA" value="positieve" />
          <node concept="2UK0tq" id="6dPxXIRBLqh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLqf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLqc" role="19SJt6">
          <property role="19SUeA" value="bijdrage" />
          <node concept="2UK0tq" id="6dPxXIRBLqd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLqb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLq8" role="19SJt6">
          <property role="19SUeA" value="levert" />
          <node concept="2UK0tq" id="6dPxXIRBLq9" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLq7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLq4" role="19SJt6">
          <property role="19SUeA" value="aan" />
          <node concept="2UK0tq" id="6dPxXIRBLq5" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLq3" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLq0" role="19SJt6">
          <property role="19SUeA" value="natuur" />
          <node concept="2UK0tq" id="6dPxXIRBLq1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLpZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLpW" role="19SJt6">
          <property role="19SUeA" value="en" />
          <node concept="2UK0tq" id="6dPxXIRBLpX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLpV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLpS" role="19SJt6">
          <property role="19SUeA" value="milieu" />
          <node concept="2UK0tq" id="6dPxXIRBLpT" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLpR" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLpO" role="19SJt6">
          <property role="19SUeA" value="(ecology);" />
          <node concept="2UK0tq" id="6dPxXIRBLpP" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLpN" role="19SJt6" />
      </node>
    </node>
    <node concept="1zEXH2" id="6dPxXIRBLDo" role="coNO9">
      <node concept="1FQA6B" id="6dPxXIRBLDw" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$i" resolve="geen punten toegekend indien niet wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLDC" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$u" resolve="één punt toegekend indien er in zeer geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLDL" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$E" resolve="twee punten toegekend indien er in geringe mate wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLDV" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL$Q" resolve="drie punten toegekend indien gemiddeld wordt voldaan aan het criterium" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLE6" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_2" resolve="vier punten toegekend indien in ruime mate wordt voldaan aan het criterium " />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLEi" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBL_h" resolve="vijf punten toegekend indien in zeer ruime mate wordt voldaan aan het criterium" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLoY">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="toekenning van punten voor de mate waarin de mate waarin de activiteit een positieve bijdrage levert aan het zichtbaarheid van het Frysk en/of de Friese streektalen" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLoZ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="toekenning van punten voor de mate waarin de mate waarin de activiteit een positieve bijdrage levert aan het zichtbaarheid van het Frysk en/of de Friese streektalen" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLpe" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 onderdeel f Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLpf" role="2hN6Sa">
        <node concept="2h$EKm" id="6dPxXIRBLXM" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLXN" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLXL" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLXJ" role="19SJt6">
          <property role="19SUeA" value="mate" />
          <node concept="2UK0tq" id="6dPxXIRBLXK" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLXI" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLXF" role="19SJt6">
          <property role="19SUeA" value="waarin" />
          <node concept="2UK0tq" id="6dPxXIRBLXG" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLXE" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLs$" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLs_" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLsz" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLsw" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6dPxXIRBLsx" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLsv" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLss" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6dPxXIRBLst" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLsr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLso" role="19SJt6">
          <property role="19SUeA" value="positieve" />
          <node concept="2UK0tq" id="6dPxXIRBLsp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLsn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLsk" role="19SJt6">
          <property role="19SUeA" value="bijdrage" />
          <node concept="2UK0tq" id="6dPxXIRBLsl" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLsj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLsg" role="19SJt6">
          <property role="19SUeA" value="levert" />
          <node concept="2UK0tq" id="6dPxXIRBLsh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLsf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLsc" role="19SJt6">
          <property role="19SUeA" value="aan" />
          <node concept="2UK0tq" id="6dPxXIRBLsd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLsb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLs8" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6dPxXIRBLs9" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLs7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLs4" role="19SJt6">
          <property role="19SUeA" value="zichtbaarheid" />
          <node concept="2UK0tq" id="6dPxXIRBLs5" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLs3" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLs0" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6dPxXIRBLs1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLrZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLrW" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6dPxXIRBLrX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLrV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLrS" role="19SJt6">
          <property role="19SUeA" value="Frysk" />
          <node concept="2UK0tq" id="6dPxXIRBLrT" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLrR" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLrO" role="19SJt6">
          <property role="19SUeA" value="en/of" />
          <node concept="2UK0tq" id="6dPxXIRBLrP" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLrK" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6dPxXIRBLrL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLrJ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLrG" role="19SJt6">
          <property role="19SUeA" value="Friese" />
          <node concept="2UK0tq" id="6dPxXIRBLrH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLrF" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6dPxXIRBLrC" role="19SJt6">
          <property role="19SUeA" value="streektalen." />
          <node concept="2UK0tq" id="6dPxXIRBLrD" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6dPxXIRBLrB" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1zEXH2" id="6dPxXIRBLEy" role="coNO9">
      <node concept="1FQA6B" id="6dPxXIRBLEL" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLEJ" resolve="geen punten toegekend indien het project niets doet met het Fries en/of de in het gebied gebruikte Friese streektaal" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLEY" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLEW" resolve="één punt toegekend indien het project het Fries en/of de in het gebied gebruikte Friese streektaal meer zichtbaar maakt door middel van voor de hand liggende zaken" />
      </node>
      <node concept="1FQA6B" id="6dPxXIRBLFd" role="1zF96y">
        <ref role="1FQA6$" node="6dPxXIRBLFb" resolve="twee punten toegekend indien het project bovengemiddeld bijdraagt aan de zichtbaarheid van het Fries en/of de in het gebied gebruikte Friese streektaal" />
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL$i">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="geen punten toegekend indien niet wordt voldaan aan het criterium" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL$j" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="geen punten toegekend indien niet wordt voldaan aan het criterium" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLGt" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 3 onderdeel a Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLGu" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLGv" role="19SJt6">
          <property role="19SUeA" value="geen punten toegekend indien niet wordt voldaan aan het criterium;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL$u">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="één punt toegekend indien er in zeer geringe mate wordt voldaan aan het criterium" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL$v" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="één punt toegekend indien er in zeer geringe mate wordt voldaan aan het criterium" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLGI" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 3 onderdeel b Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLGJ" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLGK" role="19SJt6">
          <property role="19SUeA" value="één punt toegekend indien er in zeer geringe mate wordt voldaan aan het criterium;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL$E">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="twee punten toegekend indien er in geringe mate wordt voldaan aan het criterium" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL$F" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="twee punten toegekend indien er in geringe mate wordt voldaan aan het criterium" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLGZ" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 3 onderdeel c Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLH0" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLH1" role="19SJt6">
          <property role="19SUeA" value="twee punten toegekend indien er in geringe mate wordt voldaan aan het criterium;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL$Q">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="drie punten toegekend indien gemiddeld wordt voldaan aan het criterium" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL$R" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="drie punten toegekend indien gemiddeld wordt voldaan aan het criterium" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLHj" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 3 onderdeel d Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLHk" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLHl" role="19SJt6">
          <property role="19SUeA" value="drie punten toegekend indien gemiddeld wordt voldaan aan het criterium;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL_2">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="vier punten toegekend indien in ruime mate wordt voldaan aan het criterium " />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL_3" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="vier punten toegekend indien in ruime mate wordt voldaan aan het criterium " />
    </node>
    <node concept="cog_b" id="6dPxXIRBLH$" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 3 onderdeel e Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLH_" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLHA" role="19SJt6">
          <property role="19SUeA" value="vier punten toegekend indien in ruime mate wordt voldaan aan het criterium;" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBL_h">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="vijf punten toegekend indien in zeer ruime mate wordt voldaan aan het criterium" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBL_i" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="vijf punten toegekend indien in zeer ruime mate wordt voldaan aan het criterium" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLHP" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 3 onderdeel f Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLHQ" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLHR" role="19SJt6">
          <property role="19SUeA" value="vijf punten toegekend indien in zeer ruime mate wordt voldaan aan het criterium." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLEJ">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="geen punten toegekend indien het project niets doet met het Fries en/of de in het gebied gebruikte Friese streektaal" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLEK" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="geen punten toegekend indien het project niets doet met het Fries en/of de in het gebied gebruikte Friese streektaal" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLFy" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 4 onderdeel a Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLFz" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLF$" role="19SJt6">
          <property role="19SUeA" value="geen punten toegekend indien het project niets doet met het Fries en/of de in het gebied gebruikte&#10;Friese streektaal." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLEW">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="één punt toegekend indien het project het Fries en/of de in het gebied gebruikte Friese streektaal meer zichtbaar maakt door middel van voor de hand liggende zaken" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLEX" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="één punt toegekend indien het project het Fries en/of de in het gebied gebruikte Friese streektaal meer zichtbaar maakt door middel van voor de hand liggende zaken" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLFP" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 4 onderdeel b Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLFQ" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLFR" role="19SJt6">
          <property role="19SUeA" value="één punt toegekend indien het project het Fries en/of de in het gebied gebruikte Friese streektaal meer zichtbaar maakt door middel van voor de hand liggende zaken." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6dPxXIRBLFb">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="twee punten toegekend indien het project bovengemiddeld bijdraagt aan de zichtbaarheid van het Fries en/of de in het gebied gebruikte Friese streektaal" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6dPxXIRBLFc" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="twee punten toegekend indien het project bovengemiddeld bijdraagt aan de zichtbaarheid van het Fries en/of de in het gebied gebruikte Friese streektaal" />
    </node>
    <node concept="cog_b" id="6dPxXIRBLG1" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 4 onderdeel c Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6dPxXIRBLG2" role="2hN6Sa">
        <node concept="19SUe$" id="6dPxXIRBLG3" role="19SJt6">
          <property role="19SUeA" value="twee punten toegekend indien het project bovengemiddeld bijdraagt aan de zichtbaarheid van het Fries en/of de in het gebied gebruikte Friese streektaal." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVys0">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aantal punten toegekend aan aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1RnfdX" id="6OGuHoEVys1" role="coNO9" />
    <node concept="1GVOM6" id="6OGuHoEVys2" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aantal punten toegekend aan aanvraag subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyte">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="toegekend" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVytf" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="toekennen" />
    </node>
    <node concept="cog_b" id="6OGuHoEVytl" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 1 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVytm" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVytn" role="19SJt6">
          <property role="19SUeA" value="Ten behoeve van de rangschikking worden maximaal 27 punten " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyud" role="19SJt6">
          <property role="19SUeA" value="toegekend" />
          <node concept="2UK0tq" id="6OGuHoEVyue" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyuc" role="19SJt6">
          <property role="19SUeA" value=" met de volgende maxima per criterium:" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyw8">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="punten" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVyw9" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="punten" />
    </node>
    <node concept="cog_b" id="6OGuHoEVywf" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 2 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVywg" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVywh" role="19SJt6">
          <property role="19SUeA" value="Ten behoeve van de rangschikking worden maximaal 27 " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyws" role="19SJt6">
          <property role="19SUeA" value="punten" />
          <node concept="2UK0tq" id="6OGuHoEVywt" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVywu" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVywv" role="19SJt6">
          <property role="19SUeA" value="toegekend" />
          <node concept="2UK0tq" id="6OGuHoEVyww" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVywx" role="19SJt6">
          <property role="19SUeA" value=" met de volgende maxima per criterium:" />
        </node>
      </node>
    </node>
  </node>
  <node concept="mu5$5" id="6OGuHoEVywT">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="verdelen van het beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân " />
    <property role="3ANC2_" value="1) Hoe zit het hier met de gebieden?&#10;&#10;2) Is provinciale staten hier de recipient, en zo ja waarom?" />
    <node concept="1GVOM6" id="6OGuHoEVywU" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="verdelen van het beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân " />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVyxe" role="3FTnq6">
      <ref role="1FQA6$" node="6OGuHoEVyxf" resolve="verdelen" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVyxx" role="3H36mW">
      <ref role="1FQA6$" node="7614aO2F_oB" resolve="gedeputeerde staten" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVyy9" role="3H36l7">
      <ref role="1FQA6$" node="6OGuHoEVyya" resolve="voor:" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVyyW" role="3H36lm">
      <ref role="1FQA6$" node="6dPxXIRBJGa" resolve="provinciale staten" />
    </node>
    <node concept="cog_b" id="6OGuHoEVyyX" role="2pmM46">
      <property role="2XObfb" value="Artikel 2 aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVyyY" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyyZ" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten kunnen ter bevordering van de leefbaarheid in de provincie Fryslân subsidie verstrekken voor:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVyzr" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 5 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVyzs" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyzt" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten verdelen het beschikbare bedrag in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVyH0" role="mu1cf">
      <ref role="1FQA6$" node="6OGuHoEVyGX" resolve="verdeling beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="1zEWgd" id="6OGuHoEVyHT" role="mu3T0">
      <node concept="1FQA6B" id="6OGuHoEVyMZ" role="1zF96y">
        <ref role="1FQA6$" node="6OGuHoEVyMX" resolve="het beschikbare subsidiebedrag wordt verdeeld in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord" />
      </node>
      <node concept="1zEXH2" id="6OGuHoEVyNd" role="1zF96y">
        <node concept="1FQA6B" id="6OGuHoEVyNr" role="1zF96y">
          <ref role="1FQA6$" node="6OGuHoEVyNp" resolve="de aanvraag die het subsidieplafond overschrijdt ontvangt het subsidiebedrag dat nog beschikbaar is" />
        </node>
        <node concept="1zEWgd" id="6OGuHoEVyNA" role="1zF96y">
          <node concept="1FQA6B" id="6OGuHoEVyNO" role="1zF96y">
            <ref role="1FQA6$" node="6OGuHoEVyNM" resolve="het subsidieplafond wordt overschreden door verstrekking van subsidie voor aanvragen die even hoog zijn gerangschikt" />
          </node>
          <node concept="1FQA6B" id="6OGuHoEVyO1" role="1zF96y">
            <ref role="1FQA6$" node="6OGuHoEVyNZ" resolve="door middel van loting wordt vastgesteld welke van de even hoog gerangschikte aanvragen die het subsidieplafond overschreden subsidie ontvangt" />
          </node>
          <node concept="1FQA6B" id="6OGuHoEVyOI" role="1zF96y">
            <ref role="1FQA6$" node="6OGuHoEVyOG" resolve="de aanvraag die de loting wint ontvangt het subsidiebedrag dat nog beschikbaar is" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyxf">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="verdelen" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVyxg" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="verdelen" />
    </node>
    <node concept="cog_b" id="6OGuHoEVyxh" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 5 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVyxi" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyxj" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten " />
        </node>
        <node concept="19SUe$" id="6OGuHoEVyxn" role="19SJt6">
          <property role="19SUeA" value="verdelen" />
        </node>
        <node concept="19SUe$" id="6OGuHoEVyxm" role="19SJt6">
          <property role="19SUeA" value=" het beschikbare bedrag in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyya">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="voor:" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVyyb" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="het beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="cog_b" id="6OGuHoEVy_J" role="2pmM46">
      <property role="2XObfb" value="Artikel 2 aanhef Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVy_K" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVy_L" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten kunnen " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyDs" role="19SJt6">
          <property role="19SUeA" value="ter" />
          <node concept="2UK0tq" id="6OGuHoEVyDt" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyDr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyDo" role="19SJt6">
          <property role="19SUeA" value="bevordering" />
          <node concept="2UK0tq" id="6OGuHoEVyDp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyDn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyDk" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6OGuHoEVyDl" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyDj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyDg" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVyDh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyDf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyDc" role="19SJt6">
          <property role="19SUeA" value="leefbaarheid" />
          <node concept="2UK0tq" id="6OGuHoEVyDd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyDb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyD8" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6OGuHoEVyD9" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyD7" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyD4" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVyD5" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyD3" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyD0" role="19SJt6">
          <property role="19SUeA" value="provincie" />
          <node concept="2UK0tq" id="6OGuHoEVyD1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyCZ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyCW" role="19SJt6">
          <property role="19SUeA" value="Fryslân" />
          <node concept="2UK0tq" id="6OGuHoEVyCX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyCV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyCS" role="19SJt6">
          <property role="19SUeA" value="subsidie" />
          <node concept="2UK0tq" id="6OGuHoEVyCT" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyCR" role="19SJt6">
          <property role="19SUeA" value=" verstrekken " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyFX" role="19SJt6">
          <property role="19SUeA" value="voor:" />
          <node concept="2UK0tq" id="6OGuHoEVyFY" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyFW" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVy_M" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 5 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVy_N" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyA3" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten verdelen " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyC_" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6OGuHoEVyCA" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyC$" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyCx" role="19SJt6">
          <property role="19SUeA" value="beschikbare" />
          <node concept="2UK0tq" id="6OGuHoEVyCy" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyCw" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyCt" role="19SJt6">
          <property role="19SUeA" value="bedrag" />
          <node concept="2UK0tq" id="6OGuHoEVyCu" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyCs" role="19SJt6">
          <property role="19SUeA" value=" in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyGX">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="verdeling beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3GE5qa" value="facts" />
    <node concept="1RnfdX" id="6OGuHoEVyGY" role="coNO9" />
    <node concept="1GVOM6" id="6OGuHoEVyGZ" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="besluit tot verdeling beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyMX">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="het beschikbare subsidiebedrag wordt verdeeld in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord" />
    <property role="3GE5qa" value="facts" />
    <property role="3ANC2_" value="Welk bedrag wordt er aan een project toegekend? En welke rol spelen de maximale subsidies (artikel 11) voor kleine, middelgrote en grote maatschappelijke initiatieven (artikel 2) daarbij?" />
    <node concept="1GVOM6" id="6OGuHoEVyMY" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="het beschikbare subsidiebedrag wordt verdeeld in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord" />
    </node>
    <node concept="cog_b" id="6OGuHoEV$kb" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 5 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$kc" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$kd" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten verdelen het beschikbare bedrag in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyNp">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="de aanvraag die het subsidieplafond overschrijdt ontvangt het subsidiebedrag dat nog beschikbaar is" />
    <property role="3GE5qa" value="facts" />
    <property role="3ANC2_" value="In artikel 8 lid 5 staat niet expliciet dat de aanvraag die het subsidieplafond overschrijdt het subsidiebedrag ontvangt dat nog beschikbaar is. Onze interpretatie is dat artikel 8 lid 5 dit wel impliceert." />
    <node concept="1GVOM6" id="6OGuHoEVyNq" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="de aanvraag die het subsidieplafond overschrijdt ontvangt het subsidiebedrag dat nog beschikbaar is" />
    </node>
    <node concept="cog_b" id="6OGuHoEVyOP" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 5 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVyOQ" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyOR" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten verdelen het beschikbare bedrag in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyNM">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="het subsidieplafond wordt overschreden door verstrekking van subsidie voor aanvragen die even hoog zijn gerangschikt" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVyNN" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="het subsidieplafond wordt overschreden door verstrekking van subsidie voor aanvragen die even hoog zijn gerangschikt" />
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyNZ">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="door middel van loting wordt vastgesteld welke van de even hoog gerangschikte aanvragen die het subsidieplafond overschreden subsidie ontvangt" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVyO0" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="door middel van loting wordt vastgesteld welke van de even hoog gerangschikte aanvragen die het subsidieplafond overschreden subsidie ontvangt" />
    </node>
    <node concept="cog_b" id="6OGuHoEVySN" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 6 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVySO" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVySP" role="19SJt6">
          <property role="19SUeA" value="Voor zover door verstrekking van subsidie voor " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyTq" role="19SJt6">
          <property role="19SUeA" value="aanvragen," />
          <node concept="2UK0tq" id="6OGuHoEVyTr" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyTp" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyTm" role="19SJt6">
          <property role="19SUeA" value="die" />
          <node concept="2UK0tq" id="6OGuHoEVyTn" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyTl" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyTi" role="19SJt6">
          <property role="19SUeA" value="even" />
          <node concept="2UK0tq" id="6OGuHoEVyTj" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyTh" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyTe" role="19SJt6">
          <property role="19SUeA" value="hoog" />
          <node concept="2UK0tq" id="6OGuHoEVyTf" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyTd" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyTa" role="19SJt6">
          <property role="19SUeA" value="zijn" />
          <node concept="2UK0tq" id="6OGuHoEVyTb" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyT9" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyT6" role="19SJt6">
          <property role="19SUeA" value="gerangschikt," />
          <node concept="2UK0tq" id="6OGuHoEVyT7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyT5" role="19SJt6">
          <property role="19SUeA" value=" het subsidieplafond wordt overschreden, " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyU_" role="19SJt6">
          <property role="19SUeA" value="wordt" />
          <node concept="2UK0tq" id="6OGuHoEVyUA" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyU$" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyUx" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVyUy" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyUw" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyUt" role="19SJt6">
          <property role="19SUeA" value="onderlinge" />
          <node concept="2UK0tq" id="6OGuHoEVyUu" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyUs" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyUp" role="19SJt6">
          <property role="19SUeA" value="rangschikking" />
          <node concept="2UK0tq" id="6OGuHoEVyUq" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyUo" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyUl" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6OGuHoEVyUm" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyUk" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyUh" role="19SJt6">
          <property role="19SUeA" value="die" />
          <node concept="2UK0tq" id="6OGuHoEVyUi" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyUg" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyUd" role="19SJt6">
          <property role="19SUeA" value="aanvragen" />
          <node concept="2UK0tq" id="6OGuHoEVyUe" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyUc" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyU9" role="19SJt6">
          <property role="19SUeA" value="vastgesteld" />
          <node concept="2UK0tq" id="6OGuHoEVyUa" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyU8" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyU5" role="19SJt6">
          <property role="19SUeA" value="door" />
          <node concept="2UK0tq" id="6OGuHoEVyU6" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyU4" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyU1" role="19SJt6">
          <property role="19SUeA" value="middel" />
          <node concept="2UK0tq" id="6OGuHoEVyU2" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyU0" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyTX" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6OGuHoEVyTY" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyTW" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyTT" role="19SJt6">
          <property role="19SUeA" value="loting." />
          <node concept="2UK0tq" id="6OGuHoEVyTU" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyTS" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyOG">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="de aanvraag die de loting wint ontvangt het subsidiebedrag dat nog beschikbaar is" />
    <property role="3GE5qa" value="facts" />
    <property role="3ANC2_" value="In artikel 8 lid 5 staat niet expliciet dat de aanvraag die de loting wint het subsidiebedrag ontvangt dat nog beschikbaar is. Onze interpretatie is dat artikel 8 lid 5 dit wel impliceert." />
    <node concept="1GVOM6" id="6OGuHoEVyOH" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="de aanvraag die de loting wint ontvangt het subsidiebedrag dat nog beschikbaar is" />
    </node>
    <node concept="cog_b" id="6OGuHoEVyRL" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 5 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVyRM" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyRN" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten verdelen het beschikbare bedrag in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyWz">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvraag staat op besluit tot verdeling beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    <property role="3GE5qa" value="facts" />
    <property role="3ANC2_" value="" />
    <node concept="1GVOM6" id="6OGuHoEVyW$" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvraag staat op besluit tot verdeling beschikbare bedrag voor subsidie ter bevordering van de leefbaarheid in de provincie Fryslân" />
    </node>
    <node concept="cog_b" id="6OGuHoEVyX5" role="2pmM46">
      <property role="2XObfb" value="Artikel 8 lid 5 Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVyX6" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyX7" role="19SJt6">
          <property role="19SUeA" value="Gedeputeerde staten verdelen het beschikbare bedrag in de volgorde van de rangschikking voor aanvragen die minimaal 10 punten hebben gescoord." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyYQ">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit is in technische zin haalbaar" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVyYR" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit is in technische zin haalbaar" />
    </node>
    <node concept="cog_b" id="6OGuHoEVyYS" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel f Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVyYT" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyYU" role="19SJt6">
          <property role="19SUeA" value="er gegronde reden bestaat dat de " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyYV" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6OGuHoEVyYW" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyYX" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyYY" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6OGuHoEVyYZ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZ0" role="19SJt6">
          <property role="19SUeA" value=" financiële, organisatorische " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyZ7" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6OGuHoEVyZ8" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZ9" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyZa" role="19SJt6">
          <property role="19SUeA" value="technische" />
          <node concept="2UK0tq" id="6OGuHoEVyZb" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZc" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyZd" role="19SJt6">
          <property role="19SUeA" value="zin" />
          <node concept="2UK0tq" id="6OGuHoEVyZe" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZf" role="19SJt6">
          <property role="19SUeA" value=" niet " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyZg" role="19SJt6">
          <property role="19SUeA" value="haalbaar" />
          <node concept="2UK0tq" id="6OGuHoEVyZh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZi" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyZj" role="19SJt6">
          <property role="19SUeA" value="is;" />
          <node concept="2UK0tq" id="6OGuHoEVyZk" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZl" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVyZs">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit in financiële zin haalbaar" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVyZt" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit in financiële zin haalbaar" />
    </node>
    <node concept="cog_b" id="6OGuHoEVyZu" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel f Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVyZv" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVyZw" role="19SJt6">
          <property role="19SUeA" value="er gegronde reden bestaat dat de " />
        </node>
        <node concept="2h$EKm" id="3HnCquLlzzz" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="3HnCquLlzz$" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="3HnCquLlzzy" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="3HnCquLlzzv" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="3HnCquLlzzw" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="3HnCquLlzzu" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="3HnCquLlzzr" role="19SJt6">
          <property role="19SUeA" value="financiële," />
          <node concept="2UK0tq" id="3HnCquLlzzs" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="3HnCquLlzzq" role="19SJt6">
          <property role="19SUeA" value=" organisatorische of technische " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyZN" role="19SJt6">
          <property role="19SUeA" value="zin" />
          <node concept="2UK0tq" id="6OGuHoEVyZO" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZP" role="19SJt6">
          <property role="19SUeA" value=" niet " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyZQ" role="19SJt6">
          <property role="19SUeA" value="haalbaar" />
          <node concept="2UK0tq" id="6OGuHoEVyZR" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZS" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVyZT" role="19SJt6">
          <property role="19SUeA" value="is;" />
          <node concept="2UK0tq" id="6OGuHoEVyZU" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVyZV" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVz18">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="activiteit in is organisatorische zin haalbaar" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVz19" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="activiteit is in organisatorische zin haalbaar" />
    </node>
    <node concept="cog_b" id="6OGuHoEVz1a" role="2pmM46">
      <property role="2XObfb" value="Artikel 7, onderdeel f Subsidieregeling IMF 2020 Fryslân" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVz1b" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz1c" role="19SJt6">
          <property role="19SUeA" value="er gegronde reden bestaat dat de " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz1d" role="19SJt6">
          <property role="19SUeA" value="activiteit" />
          <node concept="2UK0tq" id="6OGuHoEVz1e" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz1f" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz1g" role="19SJt6">
          <property role="19SUeA" value="in" />
          <node concept="2UK0tq" id="6OGuHoEVz1h" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz1i" role="19SJt6">
          <property role="19SUeA" value=" financiële, " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz1j" role="19SJt6">
          <property role="19SUeA" value="organisatorische" />
          <node concept="2UK0tq" id="6OGuHoEVz1k" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz1l" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz1m" role="19SJt6">
          <property role="19SUeA" value="of" />
          <node concept="2UK0tq" id="6OGuHoEVz1n" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz1o" role="19SJt6">
          <property role="19SUeA" value=" technische " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz1s" role="19SJt6">
          <property role="19SUeA" value="zin" />
          <node concept="2UK0tq" id="6OGuHoEVz1t" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz1u" role="19SJt6">
          <property role="19SUeA" value=" niet " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz1v" role="19SJt6">
          <property role="19SUeA" value="haalbaar" />
          <node concept="2UK0tq" id="6OGuHoEVz1w" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz1x" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz1y" role="19SJt6">
          <property role="19SUeA" value="is;" />
          <node concept="2UK0tq" id="6OGuHoEVz1z" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz1$" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="mu5$5" id="6OGuHoEVz3P">
    <property role="2CxiQ9" value="0" />
    <property role="3GE5qa" value="acts" />
    <property role="TrG5h" value="verzoek van een belanghebbende tot het nemen van een besluit door bestuursorgaan" />
    <node concept="1GVOM6" id="6OGuHoEVz3Q" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="verzoek van een belanghebbende tot het nemen van een besluit door bestuursorgaan" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVz43" role="3H36mW">
      <ref role="1FQA6$" node="6OGuHoEVz44" resolve="belanghebbende," />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVz4p" role="3FTnq6">
      <ref role="1FQA6$" node="6OGuHoEVz4q" resolve="verzoek" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVz5g" role="3H36l7">
      <ref role="1FQA6$" node="6OGuHoEVz5h" resolve="besluit" />
    </node>
    <node concept="cog_b" id="6OGuHoEVz5y" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVz5z" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz5$" role="19SJt6">
          <property role="19SUeA" value="Onder besluit wordt verstaan: een schriftelijke beslissing van een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz7g" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan," />
          <node concept="2UK0tq" id="6OGuHoEVz7h" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBx/Recipient" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzel" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz7f" role="19SJt6">
          <property role="19SUeA" value=" inhoudende een publiekrechtelijke rechtshandeling." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVz5M" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 3 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVz5N" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz5O" role="19SJt6">
          <property role="19SUeA" value="Onder aanvraag wordt verstaan: een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz67" role="19SJt6">
          <property role="19SUeA" value="verzoek" />
          <node concept="2UK0tq" id="6OGuHoEVz68" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzd5" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz69" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzd3" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6OGuHoEVzd4" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzd2" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzcZ" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6OGuHoEVzd0" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzcY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz6a" role="19SJt6">
          <property role="19SUeA" value="belanghebbende," />
          <node concept="2UK0tq" id="6OGuHoEVz6b" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAL/Actor" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzcW" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz6c" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzdM" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6OGuHoEVzdN" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzdL" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz6d" role="19SJt6">
          <property role="19SUeA" value="besluit" />
          <node concept="2UK0tq" id="6OGuHoEVz6e" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBp/Object" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzdJ" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz6f" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzdH" role="19SJt6">
          <property role="19SUeA" value="te" />
          <node concept="2UK0tq" id="6OGuHoEVzdI" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzdG" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzdD" role="19SJt6">
          <property role="19SUeA" value="nemen." />
          <node concept="2UK0tq" id="6OGuHoEVzdE" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzdC" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVz7i" role="3H36lm">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVz7Q" role="mu1cf">
      <ref role="1FQA6$" node="6OGuHoEVz7N" resolve="aanvraag" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEVz8g" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVz8e" resolve="plicht tot schriftelijk indienen aanvraag " />
    </node>
    <node concept="2cz2WB" id="6OGuHoEVzh6" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzf1" resolve="wordt ondertekend" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEVzkx" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzhO" resolve="plicht tot vermelden naam en het adres van de aanvrager" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEVzmp" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzkH" resolve="plicht tot vermelden dagtekening aanvraag" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEVzpz" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzmA" resolve="plicht tot het vermelden van de aanduiding van het besluit dat wordt gevraagd" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEVzFy" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzFw" resolve="plicht tot verschaffen gegevens en bescheiden die voor de beslissing op de aanvraag nodig zijn" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEVzLP" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzLN" resolve="plicht bestuursorgaan tot vergaren van de nodige kennis omtrent de relevante feiten en de af te wegen belangen" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEVzUr" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEVzUp" resolve="plicht tot deugdelijk motiveren besluit" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$0t" role="mu1cf">
      <ref role="2cz2WA" node="6OGuHoEV$0r" resolve="plicht tot nemen besluit binnen de bij wettelijk voorschrift bepaalde termijn" />
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVz44">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="belanghebbende," />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVz45" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="belanghebbende" />
    </node>
    <node concept="cog_b" id="6OGuHoEVz46" role="2pmM46">
      <property role="2XObfb" value="Source missing" />
      <property role="1tl0gq" value="English" />
      <property role="1hTq4$" value="https://fin.triply.cc/ole/BWB/id/BWBR0005537/2816734/2020-07-01/2020-07-01/structuurkenmerk/67" />
      <node concept="2hPCcK" id="6OGuHoEVz47" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz48" role="19SJt6">
          <property role="19SUeA" value="Onder aanvraag wordt verstaan: een verzoek van een belanghebbende, een besluit te nemen." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVz4q">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="verzoek" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVz4r" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="verzoeken" />
    </node>
    <node concept="cog_b" id="6OGuHoEVz4s" role="2pmM46">
      <property role="2XObfb" value="Source missing" />
      <property role="1tl0gq" value="English" />
      <property role="1hTq4$" value="https://fin.triply.cc/ole/BWB/id/BWBR0005537/2816734/2020-07-01/2020-07-01/structuurkenmerk/67" />
      <node concept="2hPCcK" id="6OGuHoEVz4t" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz4u" role="19SJt6">
          <property role="19SUeA" value="Onder aanvraag wordt verstaan: een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz4W" role="19SJt6">
          <property role="19SUeA" value="verzoek" />
          <node concept="2UK0tq" id="6OGuHoEVz4X" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz4V" role="19SJt6">
          <property role="19SUeA" value=" van een belanghebbende, een besluit te nemen." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVz5h">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="besluit" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVz5i" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="besluit" />
    </node>
    <node concept="cog_b" id="6OGuHoEVz5j" role="2pmM46">
      <property role="2XObfb" value="Source missing" />
      <property role="1tl0gq" value="English" />
      <property role="1hTq4$" value="https://fin.triply.cc/ole/BWB/id/BWBR0005537/2816734/2020-07-01/2020-07-01/structuurkenmerk/67" />
      <node concept="2hPCcK" id="6OGuHoEVz5k" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz5l" role="19SJt6">
          <property role="19SUeA" value="Onder aanvraag wordt verstaan: een verzoek van een belanghebbende, een besluit te nemen." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEV$7b" role="2pmM46">
      <property role="2XObfb" value="Artikel 3:30 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$7c" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$7d" role="19SJt6">
          <property role="19SUeA" value="Een besluit treedt niet in werking voordat het is " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$7e" role="19SJt6">
          <property role="19SUeA" value="bekendgemaakt." />
          <node concept="2UK0tq" id="6OGuHoEV$7f" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$7g" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1zEWgd" id="6OGuHoEV$fH" role="coNO9">
      <node concept="1FQA6B" id="6OGuHoEV$g5" role="1zF96y">
        <ref role="1FQA6$" node="6OGuHoEV$g3" resolve="schriftelijke beslissing van een bestuursorgaan, inhoudende een publiekrechtelijke rechtshandeling." />
      </node>
      <node concept="1zEXH2" id="6OGuHoEV$gs" role="1zF96y">
        <node concept="1FQA6B" id="6OGuHoEV$du" role="1zF96y">
          <ref role="1FQA6$" node="6OGuHoEV$ds" resolve="besluit van algemene strekking" />
        </node>
        <node concept="1FQA6B" id="6OGuHoEV$dF" role="1zF96y">
          <ref role="1FQA6$" node="6OGuHoEV$dD" resolve="besluit dat niet van algemene strekking is" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVz7N">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="aanvraag" />
    <property role="3GE5qa" value="facts" />
    <node concept="1RnfdX" id="6OGuHoEVz7O" role="coNO9" />
    <node concept="1GVOM6" id="6OGuHoEVz7P" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="aanvraag" />
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEVz8e">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="plicht tot schriftelijk indienen aanvraag " />
    <property role="3GE5qa" value="duties" />
    <node concept="1GVOM6" id="6OGuHoEVz8f" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht tot schriftelijk indienen aanvraag " />
    </node>
    <node concept="cog_b" id="6OGuHoEVz8w" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVz8x" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz8y" role="19SJt6">
          <property role="19SUeA" value="Tenzij bij wettelijk voorschrift anders is bepaald, wordt de aanvraag tot het geven van een beschikking schriftelijk ingediend bij het " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz8Z" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan" />
          <node concept="2UK0tq" id="6OGuHoEVz90" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSnkYR/Claimant" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz8Y" role="19SJt6">
          <property role="19SUeA" value=" dat bevoegd is op de aanvraag te beslissen." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVz91" role="3H37fL">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="cog_b" id="6OGuHoEVz9b" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 2 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVz9c" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVz9d" role="19SJt6">
          <property role="19SUeA" value="De " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz9E" role="19SJt6">
          <property role="19SUeA" value="aanvrager" />
          <node concept="2UK0tq" id="6OGuHoEVz9F" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSnk_W/Holder" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz9D" role="19SJt6">
          <property role="19SUeA" value=" verschaft voorts de gegevens en bescheiden die voor de beslissing op de aanvraag nodig zijn en waarover hij redelijkerwijs de beschikking kan krijgen." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVz9G" role="3H37fS">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVz92">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="bestuursorgaan" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVz93" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="bestuursorgaan" />
    </node>
    <node concept="cog_b" id="6OGuHoEVzP1" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzP2" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzP3" role="19SJt6">
          <property role="19SUeA" value="Onder besluit wordt verstaan: een schriftelijke beslissing van een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzP4" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan," />
          <node concept="2UK0tq" id="6OGuHoEVzP5" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBx/Recipient" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzP6" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzWx" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzP7" role="19SJt6">
          <property role="19SUeA" value=" inhoudende een publiekrechtelijke rechtshandeling." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVzP$" role="2pmM46">
      <property role="2XObfb" value="Artikel 3:2 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzP_" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzPA" role="19SJt6">
          <property role="19SUeA" value="Bij de voorbereiding van een besluit vergaart het " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzPB" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan" />
          <node concept="2UK0tq" id="6OGuHoEVzPC" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSnk_W/Holder" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzWL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzPD" role="19SJt6">
          <property role="19SUeA" value=" de nodige kennis omtrent de relevante feiten en de af te wegen belangen." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVzOA" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzOB" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzOC" role="19SJt6">
          <property role="19SUeA" value="Tenzij bij wettelijk voorschrift anders is bepaald, wordt de aanvraag tot het geven van een beschikking schriftelijk ingediend bij het " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzX5" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan" />
          <node concept="2UK0tq" id="6OGuHoEVzX6" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzX4" role="19SJt6">
          <property role="19SUeA" value=" dat bevoegd is op de aanvraag te beslissen." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEVzci">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="belanghebbende die bestuursorgaan verzoekt tot het nemen van een besluit" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEVzcj" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="belanghebbende die bestuursorgaan verzoekt tot het nemen van een besluit" />
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEVzf1">
    <property role="2CxiQ9" value="0" />
    <property role="3GE5qa" value="duties" />
    <property role="TrG5h" value="wordt ondertekend" />
    <node concept="1GVOM6" id="6OGuHoEVzf2" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht tot ondertekenen aanvraag" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzf9" role="3H37fS">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzgs" role="3H37fL">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="cog_b" id="6OGuHoEVzDL" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 1, aanhef Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzDM" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzDN" role="19SJt6">
          <property role="19SUeA" value="De " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzEB" role="19SJt6">
          <property role="19SUeA" value="aanvraag" />
          <node concept="2UK0tq" id="6OGuHoEVzEC" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzEA" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzEW" role="19SJt6">
          <property role="19SUeA" value="wordt" />
          <node concept="2UK0tq" id="6OGuHoEVzEX" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzEV" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzES" role="19SJt6">
          <property role="19SUeA" value="ondertekend" />
          <node concept="2UK0tq" id="6OGuHoEVzET" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzER" role="19SJt6">
          <property role="19SUeA" value=" en bevat ten minste:" />
        </node>
      </node>
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEVzhO">
    <property role="2CxiQ9" value="0" />
    <property role="3GE5qa" value="duties" />
    <property role="TrG5h" value="plicht tot vermelden naam en het adres van de aanvrager" />
    <property role="3ANC2_" value="Het werkwoord 'vermelden' is hier gebruikt om aan te duiden wat de aanvrager moet doen om de aanvraag de naam en het adres van de aanvrager te doen bevatten." />
    <node concept="1GVOM6" id="6OGuHoEVzhP" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht tot vermelden naam en het adres van de aanvrager" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzkc" role="3H37fS">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzkh" role="3H37fL">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="cog_b" id="6OGuHoEVzzG" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 1, aanhef Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzzH" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzzI" role="19SJt6">
          <property role="19SUeA" value="De aanvraag wordt ondertekend en bevat ten minste:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVzzJ" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 1, onderdeel a Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzzK" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzzL" role="19SJt6">
          <property role="19SUeA" value="de " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz$D" role="19SJt6">
          <property role="19SUeA" value="naam" />
          <node concept="2UK0tq" id="6OGuHoEVz$E" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz$F" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz$G" role="19SJt6">
          <property role="19SUeA" value="en" />
          <node concept="2UK0tq" id="6OGuHoEVz$H" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz$I" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz$J" role="19SJt6">
          <property role="19SUeA" value="het" />
          <node concept="2UK0tq" id="6OGuHoEVz$K" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz$L" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz$M" role="19SJt6">
          <property role="19SUeA" value="adres" />
          <node concept="2UK0tq" id="6OGuHoEVz$N" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz$O" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz$P" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6OGuHoEVz$Q" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz$R" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz$S" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVz$T" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz$U" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVz$V" role="19SJt6">
          <property role="19SUeA" value="aanvrager;" />
          <node concept="2UK0tq" id="6OGuHoEVz$W" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVz_6" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEVzkH">
    <property role="2CxiQ9" value="0" />
    <property role="3GE5qa" value="duties" />
    <property role="TrG5h" value="plicht tot vermelden dagtekening aanvraag" />
    <property role="3ANC2_" value="Het werkwoord 'vermelden' is hier gebruikt om aan te duiden wat de aanvrager moet doen om de aanvraag de dagtekening te doen bevatten." />
    <node concept="1GVOM6" id="6OGuHoEVzkI" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht tot vermelden dagtekening aanvraag" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzlX" role="3H37fS">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzm5" role="3H37fL">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="cog_b" id="6OGuHoEVzyB" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 1, aanhef Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzyC" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzyD" role="19SJt6">
          <property role="19SUeA" value="De aanvraag wordt ondertekend en bevat ten minste:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVzxY" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 1, onderdeel b Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzxZ" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzy0" role="19SJt6">
          <property role="19SUeA" value="de " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzyq" role="19SJt6">
          <property role="19SUeA" value="dagtekening;" />
          <node concept="2UK0tq" id="6OGuHoEVzyr" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzys" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEVzmA">
    <property role="2CxiQ9" value="0" />
    <property role="3GE5qa" value="duties" />
    <property role="TrG5h" value="plicht tot het vermelden van de aanduiding van het besluit dat wordt gevraagd" />
    <node concept="1GVOM6" id="6OGuHoEVzmB" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht tot het vermelden van de aanduiding van het besluit dat wordt gevraagd" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzmI" role="3H37fS">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzmN" role="3H37fL">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="cog_b" id="6OGuHoEVzpL" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzpM" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzpN" role="19SJt6">
          <property role="19SUeA" value="Onder besluit wordt verstaan: een schriftelijke beslissing van een bestuursorgaan, inhoudende een publiekrechtelijke rechtshandeling." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVzpP" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 2 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzpQ" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzpR" role="19SJt6">
          <property role="19SUeA" value="Onder beschikking wordt verstaan: een besluit dat niet van algemene strekking is, met inbegrip van de afwijzing van een aanvraag daarvan." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVzrW" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 1, aanhef Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzrX" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzrY" role="19SJt6">
          <property role="19SUeA" value="De aanvraag wordt ondertekend en bevat ten minste:" />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVzsx" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 1, onderdeel c Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzsy" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzsz" role="19SJt6">
          <property role="19SUeA" value="een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVztb" role="19SJt6">
          <property role="19SUeA" value="aanduiding" />
          <node concept="2UK0tq" id="6OGuHoEVztc" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVztd" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzte" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6OGuHoEVztf" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVztg" role="19SJt6">
          <property role="19SUeA" value=" de beschikking " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzth" role="19SJt6">
          <property role="19SUeA" value="die" />
          <node concept="2UK0tq" id="6OGuHoEVzti" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVztj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVztk" role="19SJt6">
          <property role="19SUeA" value="wordt" />
          <node concept="2UK0tq" id="6OGuHoEVztl" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVztm" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVztn" role="19SJt6">
          <property role="19SUeA" value="gevraagd." />
          <node concept="2UK0tq" id="6OGuHoEVzto" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzu3" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEVzFw">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="plicht tot verschaffen gegevens en bescheiden die voor de beslissing op de aanvraag nodig zijn" />
    <property role="3GE5qa" value="duties" />
    <node concept="1GVOM6" id="6OGuHoEVzFx" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht tot verschaffen gegevens en bescheiden die voor de beslissing op de aanvraag nodig zijn" />
    </node>
    <node concept="cog_b" id="6OGuHoEVzGl" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzGm" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzGn" role="19SJt6">
          <property role="19SUeA" value="Onder besluit wordt verstaan: een schriftelijke beslissing van een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzGo" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan," />
          <node concept="2UK0tq" id="6OGuHoEVzGp" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBx/Recipient" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzGq" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzHc" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSnkYR/Claimant" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzGr" role="19SJt6">
          <property role="19SUeA" value=" inhoudende een publiekrechtelijke rechtshandeling." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEVzFN" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:2 lid 2 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzFO" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzFP" role="19SJt6">
          <property role="19SUeA" value="De " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzGZ" role="19SJt6">
          <property role="19SUeA" value="aanvrager" />
          <node concept="2UK0tq" id="6OGuHoEVzH0" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSnk_W/Holder" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzGY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIM" role="19SJt6">
          <property role="19SUeA" value="verschaft" />
          <node concept="2UK0tq" id="6OGuHoEVzIN" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzIL" role="19SJt6">
          <property role="19SUeA" value=" voorts de " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIA" role="19SJt6">
          <property role="19SUeA" value="gegevens" />
          <node concept="2UK0tq" id="6OGuHoEVzIB" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzI_" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIy" role="19SJt6">
          <property role="19SUeA" value="en" />
          <node concept="2UK0tq" id="6OGuHoEVzIz" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzIx" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIu" role="19SJt6">
          <property role="19SUeA" value="bescheiden" />
          <node concept="2UK0tq" id="6OGuHoEVzIv" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzIt" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIq" role="19SJt6">
          <property role="19SUeA" value="die" />
          <node concept="2UK0tq" id="6OGuHoEVzIr" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzIp" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIm" role="19SJt6">
          <property role="19SUeA" value="voor" />
          <node concept="2UK0tq" id="6OGuHoEVzIn" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzIl" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIi" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVzIj" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzIh" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIe" role="19SJt6">
          <property role="19SUeA" value="beslissing" />
          <node concept="2UK0tq" id="6OGuHoEVzIf" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzId" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzIa" role="19SJt6">
          <property role="19SUeA" value="op" />
          <node concept="2UK0tq" id="6OGuHoEVzIb" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzI9" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzI6" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVzI7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzI5" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzI2" role="19SJt6">
          <property role="19SUeA" value="aanvraag" />
          <node concept="2UK0tq" id="6OGuHoEVzI3" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzI1" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzHY" role="19SJt6">
          <property role="19SUeA" value="nodig" />
          <node concept="2UK0tq" id="6OGuHoEVzHZ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzHX" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzHU" role="19SJt6">
          <property role="19SUeA" value="zijn" />
          <node concept="2UK0tq" id="6OGuHoEVzHV" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzHT" role="19SJt6">
          <property role="19SUeA" value=" en waarover hij redelijkerwijs de beschikking kan krijgen." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzGJ" role="3H37fS">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzHd" role="3H37fL">
      <ref role="1FQA6$" node="6OGuHoEVzHe" resolve="bestuursorgaan," />
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEVzLN">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="plicht bestuursorgaan tot vergaren van de nodige kennis omtrent de relevante feiten en de af te wegen belangen" />
    <property role="3GE5qa" value="duties" />
    <node concept="1GVOM6" id="6OGuHoEVzLO" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht bestuursorgaan tot vergaren van de nodige kennis omtrent de relevante feiten en de af te wegen belangen" />
    </node>
    <node concept="cog_b" id="6OGuHoEVzM7" role="2pmM46">
      <property role="2XObfb" value="Artikel 3:2 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzM8" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzM9" role="19SJt6">
          <property role="19SUeA" value="Bij de voorbereiding van een besluit " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzRc" role="19SJt6">
          <property role="19SUeA" value="vergaart" />
          <node concept="2UK0tq" id="6OGuHoEVzRd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzRb" role="19SJt6">
          <property role="19SUeA" value=" het " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzNx" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan" />
          <node concept="2UK0tq" id="6OGuHoEVzNy" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSnk_W/Holder" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEVzR5" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzNw" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzR3" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVzR4" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzR2" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQZ" role="19SJt6">
          <property role="19SUeA" value="nodige" />
          <node concept="2UK0tq" id="6OGuHoEVzR0" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQV" role="19SJt6">
          <property role="19SUeA" value="kennis" />
          <node concept="2UK0tq" id="6OGuHoEVzQW" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQU" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQR" role="19SJt6">
          <property role="19SUeA" value="omtrent" />
          <node concept="2UK0tq" id="6OGuHoEVzQS" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQQ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQN" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVzQO" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQM" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQJ" role="19SJt6">
          <property role="19SUeA" value="relevante" />
          <node concept="2UK0tq" id="6OGuHoEVzQK" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQI" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQF" role="19SJt6">
          <property role="19SUeA" value="feiten" />
          <node concept="2UK0tq" id="6OGuHoEVzQG" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQE" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQB" role="19SJt6">
          <property role="19SUeA" value="en" />
          <node concept="2UK0tq" id="6OGuHoEVzQC" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQA" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQz" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEVzQ$" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQy" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQv" role="19SJt6">
          <property role="19SUeA" value="af" />
          <node concept="2UK0tq" id="6OGuHoEVzQw" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQu" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQr" role="19SJt6">
          <property role="19SUeA" value="te" />
          <node concept="2UK0tq" id="6OGuHoEVzQs" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQq" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQn" role="19SJt6">
          <property role="19SUeA" value="wegen" />
          <node concept="2UK0tq" id="6OGuHoEVzQo" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQm" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzQj" role="19SJt6">
          <property role="19SUeA" value="belangen." />
          <node concept="2UK0tq" id="6OGuHoEVzQk" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzQi" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzN2" role="3H37fL">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzNz" role="3H37fS">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEVzUp">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="plicht tot deugdelijk motiveren besluit" />
    <property role="3GE5qa" value="duties" />
    <node concept="1GVOM6" id="6OGuHoEVzUq" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht tot deugdelijk motiveren besluit" />
    </node>
    <node concept="cog_b" id="6OGuHoEVzUC" role="2pmM46">
      <property role="2XObfb" value="Artikel 3:46 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEVzUD" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEVzUE" role="19SJt6">
          <property role="19SUeA" value="Een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzY$" role="19SJt6">
          <property role="19SUeA" value="besluit" />
          <node concept="2UK0tq" id="6OGuHoEVzY_" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzYz" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzYw" role="19SJt6">
          <property role="19SUeA" value="dient" />
          <node concept="2UK0tq" id="6OGuHoEVzYx" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzYv" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzYs" role="19SJt6">
          <property role="19SUeA" value="te" />
          <node concept="2UK0tq" id="6OGuHoEVzYt" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzYr" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzYo" role="19SJt6">
          <property role="19SUeA" value="berusten" />
          <node concept="2UK0tq" id="6OGuHoEVzYp" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzYn" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzYk" role="19SJt6">
          <property role="19SUeA" value="op" />
          <node concept="2UK0tq" id="6OGuHoEVzYl" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzYj" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzYg" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6OGuHoEVzYh" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzYf" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzYc" role="19SJt6">
          <property role="19SUeA" value="deugdelijke" />
          <node concept="2UK0tq" id="6OGuHoEVzYd" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzYb" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEVzY8" role="19SJt6">
          <property role="19SUeA" value="motivering." />
          <node concept="2UK0tq" id="6OGuHoEVzY9" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEVzY7" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzUX" role="3H37fS">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEVzXQ" role="3H37fL">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
  </node>
  <node concept="2cz0EU" id="6OGuHoEV$0r">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="plicht tot nemen besluit binnen de bij wettelijk voorschrift bepaalde termijn" />
    <property role="3GE5qa" value="duties" />
    <node concept="1GVOM6" id="6OGuHoEV$0s" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="plicht tot nemen besluit binnen de bij wettelijk voorschrift bepaalde termijn" />
    </node>
    <node concept="cog_b" id="6OGuHoEV$0F" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 2 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$0G" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$0H" role="19SJt6">
          <property role="19SUeA" value="Onder beschikking wordt verstaan: een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$1G" role="19SJt6">
          <property role="19SUeA" value="besluit" />
          <node concept="2UK0tq" id="6OGuHoEV$1H" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$1F" role="19SJt6">
          <property role="19SUeA" value=" dat niet van algemene strekking is, met inbegrip van de afwijzing van een aanvraag daarvan." />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEV$0T" role="3H37fS">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEV$11" role="3H37fL">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="cog_b" id="6OGuHoEV$1l" role="2pmM46">
      <property role="2XObfb" value="Artikel 4:13 lid 1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$1m" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$1n" role="19SJt6">
          <property role="19SUeA" value="Een beschikking " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$2_" role="19SJt6">
          <property role="19SUeA" value="dient" />
          <node concept="2UK0tq" id="6OGuHoEV$2A" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$2$" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$2x" role="19SJt6">
          <property role="19SUeA" value="te" />
          <node concept="2UK0tq" id="6OGuHoEV$2y" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$2w" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$2t" role="19SJt6">
          <property role="19SUeA" value="worden" />
          <node concept="2UK0tq" id="6OGuHoEV$2u" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$2s" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$2p" role="19SJt6">
          <property role="19SUeA" value="gegeven" />
          <node concept="2UK0tq" id="6OGuHoEV$2q" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$2o" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$2l" role="19SJt6">
          <property role="19SUeA" value="binnen" />
          <node concept="2UK0tq" id="6OGuHoEV$2m" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$2k" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$2h" role="19SJt6">
          <property role="19SUeA" value="de" />
          <node concept="2UK0tq" id="6OGuHoEV$2i" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$2g" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$2d" role="19SJt6">
          <property role="19SUeA" value="bij" />
          <node concept="2UK0tq" id="6OGuHoEV$2e" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$2c" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$29" role="19SJt6">
          <property role="19SUeA" value="wettelijk" />
          <node concept="2UK0tq" id="6OGuHoEV$2a" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$28" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$25" role="19SJt6">
          <property role="19SUeA" value="voorschrift" />
          <node concept="2UK0tq" id="6OGuHoEV$26" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$24" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$21" role="19SJt6">
          <property role="19SUeA" value="bepaalde" />
          <node concept="2UK0tq" id="6OGuHoEV$22" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$20" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$1X" role="19SJt6">
          <property role="19SUeA" value="termijn" />
          <node concept="2UK0tq" id="6OGuHoEV$1Y" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$1W" role="19SJt6">
          <property role="19SUeA" value=" of, bij het ontbreken van zulk een termijn, binnen een redelijke termijn na ontvangst van de aanvraag." />
        </node>
      </node>
    </node>
  </node>
  <node concept="mu5$5" id="6OGuHoEV$5a">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="bekendmaken besluit door bestuursorgaan" />
    <node concept="1GVOM6" id="6OGuHoEV$5b" role="1GVO30">
      <property role="1GVPtd" value="N" />
      <property role="1GVPtb" value="bekendmaken besluit door bestuursorgaan" />
    </node>
    <node concept="cog_b" id="6OGuHoEV$5g" role="2pmM46">
      <property role="2XObfb" value="Artikel 3:40 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$5h" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$5i" role="19SJt6">
          <property role="19SUeA" value="Een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$8o" role="19SJt6">
          <property role="19SUeA" value="besluit" />
          <node concept="2UK0tq" id="6OGuHoEV$8O" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$8n" role="19SJt6">
          <property role="19SUeA" value=" treedt niet in werking voordat het is " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$5B" role="19SJt6">
          <property role="19SUeA" value="bekendgemaakt." />
          <node concept="2UK0tq" id="6OGuHoEV$5C" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAP/Action" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$8C" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$5A" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
    <node concept="1FQA6B" id="6OGuHoEV$5r" role="3H36mW">
      <ref role="1FQA6$" node="6OGuHoEVz92" resolve="bestuursorgaan" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEV$5w" role="3H36lm">
      <ref role="1FQA6$" node="7614aO2F_jE" resolve="aanvrager" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEV$5D" role="3FTnq6">
      <ref role="1FQA6$" node="6OGuHoEV$5E" resolve="bekendgemaakt." />
    </node>
    <node concept="1FQA6B" id="6OGuHoEV$6K" role="3H36l7">
      <ref role="1FQA6$" node="6OGuHoEVz5h" resolve="besluit" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEV$7A" role="mu1cf">
      <ref role="1FQA6$" node="6OGuHoEV$7z" resolve="in werking getreden besluit" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$7I" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEVz8e" resolve="plicht tot schriftelijk indienen aanvraag " />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$7Z" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEVzf1" resolve="wordt ondertekend" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$80" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEVzhO" resolve="plicht tot vermelden naam en het adres van de aanvrager" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$81" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEVzkH" resolve="plicht tot vermelden dagtekening aanvraag" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$82" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEVzmA" resolve="plicht tot het vermelden van de aanduiding van het besluit dat wordt gevraagd" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$83" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEVzFw" resolve="plicht tot verschaffen gegevens en bescheiden die voor de beslissing op de aanvraag nodig zijn" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$84" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEVzLN" resolve="plicht bestuursorgaan tot vergaren van de nodige kennis omtrent de relevante feiten en de af te wegen belangen" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$85" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEVzUp" resolve="plicht tot deugdelijk motiveren besluit" />
    </node>
    <node concept="2cz2WB" id="6OGuHoEV$86" role="mu1c7">
      <ref role="2cz2WA" node="6OGuHoEV$0r" resolve="plicht tot nemen besluit binnen de bij wettelijk voorschrift bepaalde termijn" />
    </node>
    <node concept="1FQA6B" id="6OGuHoEV$9t" role="mu3T0">
      <ref role="1FQA6$" node="6OGuHoEV$9r" resolve="besluit dient te berusten op een deugdelijke motivering." />
    </node>
    <node concept="1GVOM6" id="3HnCquLlzvM" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="bekendmaken besluit door bestuursorgaan" />
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEV$5E">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="bekendgemaakt." />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEV$5F" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="bekendmaken" />
    </node>
    <node concept="cog_b" id="6OGuHoEV$5G" role="2pmM46">
      <property role="2XObfb" value="Artikel 3:30 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$5H" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$5I" role="19SJt6">
          <property role="19SUeA" value="Een besluit treedt niet in werking voordat het is " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$6w" role="19SJt6">
          <property role="19SUeA" value="bekendgemaakt." />
          <node concept="2UK0tq" id="6OGuHoEV$6x" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$6v" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEV$7z">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="in werking getreden besluit" />
    <property role="3GE5qa" value="facts" />
    <node concept="1RnfdX" id="6OGuHoEV$7$" role="coNO9" />
    <node concept="1GVOM6" id="6OGuHoEV$7_" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="in werking getreden besluit" />
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEV$9r">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="besluit dient te berusten op een deugdelijke motivering." />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEV$9s" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="besluit rust op een deugdelijke motivering" />
    </node>
    <node concept="cog_b" id="6OGuHoEV$9X" role="2pmM46">
      <property role="2XObfb" value="Artikel 3:46 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$9Y" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$9Z" role="19SJt6">
          <property role="19SUeA" value="Een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$a0" role="19SJt6">
          <property role="19SUeA" value="besluit" />
          <node concept="2UK0tq" id="6OGuHoEV$a1" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$bO" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$a2" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$a3" role="19SJt6">
          <property role="19SUeA" value="dient" />
          <node concept="2UK0tq" id="6OGuHoEV$a4" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$bN" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$a5" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$a6" role="19SJt6">
          <property role="19SUeA" value="te" />
          <node concept="2UK0tq" id="6OGuHoEV$a7" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$bM" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$a8" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$a9" role="19SJt6">
          <property role="19SUeA" value="berusten" />
          <node concept="2UK0tq" id="6OGuHoEV$aa" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$bL" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$ab" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$ac" role="19SJt6">
          <property role="19SUeA" value="op" />
          <node concept="2UK0tq" id="6OGuHoEV$ad" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$bK" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$ae" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$af" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6OGuHoEV$ag" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$bJ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$ah" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$ai" role="19SJt6">
          <property role="19SUeA" value="deugdelijke" />
          <node concept="2UK0tq" id="6OGuHoEV$aj" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$bI" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$ak" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$al" role="19SJt6">
          <property role="19SUeA" value="motivering." />
          <node concept="2UK0tq" id="6OGuHoEV$am" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCAbM/DutyName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$bH" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$an" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEV$ds">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="besluit van algemene strekking" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEV$dt" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="besluit van algemene strekking" />
    </node>
    <node concept="cog_b" id="6OGuHoEV$e3" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$e4" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$e5" role="19SJt6">
          <property role="19SUeA" value="Onder besluit wordt verstaan: een schriftelijke beslissing van een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$e6" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan," />
          <node concept="2UK0tq" id="6OGuHoEV$e7" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBx/Recipient" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$e8" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$e9" role="19SJt6">
          <property role="19SUeA" value=" inhoudende een publiekrechtelijke rechtshandeling." />
        </node>
      </node>
    </node>
    <node concept="cog_b" id="6OGuHoEV$eW" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 2 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$eX" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$eY" role="19SJt6">
          <property role="19SUeA" value="Onder beschikking wordt verstaan: een besluit dat niet van algemene strekking is, met inbegrip van de afwijzing van een aanvraag daarvan." />
        </node>
      </node>
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEV$dD">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="besluit dat niet van algemene strekking is" />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEV$dE" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="besluit dat niet van algemene strekking is" />
    </node>
  </node>
  <node concept="cu0$f" id="6OGuHoEV$g3">
    <property role="2CxiQ9" value="0" />
    <property role="TrG5h" value="schriftelijke beslissing van een bestuursorgaan, inhoudende een publiekrechtelijke rechtshandeling." />
    <property role="3GE5qa" value="facts" />
    <node concept="1GVOM6" id="6OGuHoEV$g4" role="1GVO30">
      <property role="1GVPtd" value="English" />
      <property role="1GVPtb" value="schriftelijke beslissing van een bestuursorgaan inhoudende een publiekrechtelijke rechtshandeling." />
    </node>
    <node concept="cog_b" id="6OGuHoEV$h2" role="2pmM46">
      <property role="2XObfb" value="Artikel 1:3 lid 1 Algemene wet bestuursrecht (01-07-2020 t/m 31-12-2020)" />
      <property role="1tl0gq" value="English" />
      <node concept="2hPCcK" id="6OGuHoEV$h3" role="2hN6Sa">
        <node concept="19SUe$" id="6OGuHoEV$h4" role="19SJt6">
          <property role="19SUeA" value="Onder besluit wordt verstaan: een " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$hZ" role="19SJt6">
          <property role="19SUeA" value="schriftelijke" />
          <node concept="2UK0tq" id="6OGuHoEV$i0" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$hY" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$hV" role="19SJt6">
          <property role="19SUeA" value="beslissing" />
          <node concept="2UK0tq" id="6OGuHoEV$hW" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$hU" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$hR" role="19SJt6">
          <property role="19SUeA" value="van" />
          <node concept="2UK0tq" id="6OGuHoEV$hS" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$hQ" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$hN" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6OGuHoEV$hO" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$hM" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$h5" role="19SJt6">
          <property role="19SUeA" value="bestuursorgaan," />
          <node concept="2UK0tq" id="6OGuHoEV$h6" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQBx/Recipient" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$h7" role="2h$EKj">
            <property role="2UK0tr" value="4AIlyP2wQAM/ActName" />
          </node>
          <node concept="2UK0tq" id="6OGuHoEV$hK" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$h8" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$hI" role="19SJt6">
          <property role="19SUeA" value="inhoudende" />
          <node concept="2UK0tq" id="6OGuHoEV$hJ" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$hH" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$hE" role="19SJt6">
          <property role="19SUeA" value="een" />
          <node concept="2UK0tq" id="6OGuHoEV$hF" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$hD" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$hA" role="19SJt6">
          <property role="19SUeA" value="publiekrechtelijke" />
          <node concept="2UK0tq" id="6OGuHoEV$hB" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$h_" role="19SJt6">
          <property role="19SUeA" value=" " />
        </node>
        <node concept="2h$EKm" id="6OGuHoEV$hy" role="19SJt6">
          <property role="19SUeA" value="rechtshandeling." />
          <node concept="2UK0tq" id="6OGuHoEV$hz" role="2h$EKj">
            <property role="2UK0tr" value="7B7tObSCA8X/FactName" />
          </node>
        </node>
        <node concept="19SUe$" id="6OGuHoEV$hx" role="19SJt6">
          <property role="19SUeA" value="" />
        </node>
      </node>
    </node>
  </node>
</model>

